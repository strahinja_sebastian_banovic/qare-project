<?php
Route::get('/', 'WelcomeController@index');

Route::get('register', 'FrontController@register');
Route::post('registerme', 'FrontController@registerme');

Route::post('resetpassword', 'FrontController@resetpassword');
Route::post('resetpasswordemail', 'FrontController@resetpasswordemail');

Route::get('login', 'FrontController@login');
Route::post('loginme', 'FrontController@loginme');

Route::get('/facebook', 'FrontController@facebook');
Route::get('/callback', 'FrontController@callback');

Route::get('verify-email/{hashemail}', 'FrontController@verifyemail')->where(['hashemail' => '.*']);;

Route::get('logout', 'FrontController@logout');
Route::get('dashboard', 'FrontController@dashboard');
Route::get('how-does-it-works', 'FrontController@howitworks');
Route::get('terms-and-conditions', 'FrontController@termsandconditions');
Route::get('help', 'FrontController@help');
Route::get('contact', 'FrontController@contact');
Route::post('contact', 'FrontController@contactus');


Route::group(['middleware' => 'App\Http\Middleware\UserMiddleware'], function()
{
	Route::get('dashboard', 'FrontController@dashboard');
	Route::get('profile', 'FrontUserController@profile');
	Route::post('profile', 'FrontUserController@editprofile');

	Route::get('activities', 'FrontActivitiesController@index');
	Route::post('activities', 'FrontActivitiesController@searchforactivity');
	Route::get('activities-search', 'FrontActivitiesController@search');
	Route::get('my-activities', 'FrontActivitiesController@myactivities');
	Route::get('activities/{id}', 'FrontActivitiesController@singleactivity');
	Route::get('book-activity/{id}', 'FrontActivitiesController@bookactivity');
	Route::get('add-activity', 'FrontActivitiesController@addactivity');
	Route::post('add-activity', 'FrontActivitiesController@addNewActivity');
	Route::get('invites', 'FrontActivitiesController@invites');
	Route::get('invites/{aid}/{bid}', 'FrontActivitiesController@invitesView');
	Route::get('invites/{aid}/{bid}/{action}', 'FrontActivitiesController@invitesViewAction');
	Route::get('invite/cancel/{id}', 'FrontActivitiesController@cancelactivity');
	Route::post('invite-comment/{id}', 'FrontActivitiesController@addcomment');

	Route::post('delete-tag', 'FrontUserController@deletetag');
	Route::get('profiles', 'FrontUserController@profiles');
	Route::get('profiles-search', 'FrontUserController@search');
	Route::get('view-profile/{id}', 'FrontUserController@viewProfile');
	Route::get('makeActivFav', 'FrontUserController@makeActivFav');
	Route::get('settings', 'FrontUserController@settings');
	Route::post('settings/notification', 'FrontUserController@notification');
	Route::post('change-email', 'FrontUserController@changeemail');
	Route::post('change-password', 'FrontUserController@changepassword');
	Route::get('delete-profile', 'FrontUserController@deleteprofile');
	Route::post('profile-view/{id}/comment', 'FrontUserController@addcomment');
	Route::post('view-profile/{id}/favorite', 'FrontUserController@favourite');
	Route::get('favorites', 'FrontUserController@favorites');
	Route::post('search-favorites', 'FrontUserController@searchFavorites');
	Route::get('favorites/{id}/remove', 'FrontUserController@removefavorite');

	Route::post('activities/{id}/chat', 'FrontChatController@chatrequestactivity');
	Route::post('view-profile/{id}/chat', 'FrontChatController@chatrequest');
	Route::get('chatrequest/{id}/{action}', 'FrontChatController@chatrequestAcceptOrDecline');
	Route::get('chatinvites', 'FrontChatController@chatinvites');
	Route::get('chatinvites/{id}/{action}', 'FrontChatController@chatinvitesAction');
	Route::get('messages', 'FrontChatController@inbox');
	Route::get('messages/{id}', 'FrontChatController@chatWithUser');
	Route::post('messages/{id}/sendmessage', 'FrontChatController@sendmessage');
	Route::post('messages/{id}/retrieveChatMessages', 'FrontChatController@retrieveChatMessages');
	Route::get('chat/retrieveCountChatMessages', 'FrontChatController@retrieveCountChatMessages');
});


Route::get('admin/login', 'AdminController@login');
Route::post('admin/loginme', 'AdminController@loginme');

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	Route::get('admin/logout', 'AdminController@logout');

	Route::get('admin/users', 'AdminUserController@index');
	Route::get('admin/user-add', 'AdminUserController@adduser');
	Route::post('admin/user-add', 'AdminUserController@addNewUser');
	Route::get('admin/users-edit/{id}', 'AdminUserController@useredit');
	Route::post('admin/users-edit/{id}', 'AdminUserController@usereditprofile');
	Route::get('admin/users-delete/{id}', 'AdminUserController@deleteuser');
	Route::post('admin/sendemail', 'AdminUserController@sendemail');

	Route::get('admin/activities', 'AdminActivityController@index');
	Route::get('admin/activity-add', 'AdminActivityController@addActivity');
	Route::post('admin/activity-add', 'AdminActivityController@addNewActivity');
	Route::get('admin/activity-edit/{id}', 'AdminActivityController@editActivity');
	Route::post('admin/activity-edit/{id}', 'AdminActivityController@editThisActivity');
	Route::get('admin/activity-delete/{id}', 'AdminActivityController@deleteActivity');

	Route::get('admin/bookings', 'AdminBookingsController@index');
	Route::get('admin/booking-add', 'AdminBookingsController@addBooking');
	Route::post('admin/booking-add', 'AdminBookingsController@addNewBooking');
	Route::get('admin/booking-edit/{id}/{uid}', 'AdminBookingsController@editBooking');
	Route::post('admin/booking-edit/{id}/{uid}', 'AdminBookingsController@editThisBooking');
	Route::get('admin/booking-delete/{id}/{uid}', 'AdminBookingsController@deleteBooking');

	Route::get('admin/questions', 'AdminQuestionController@index');
	Route::get('admin/question-add', 'AdminQuestionController@addQuestion');
	Route::post('admin/question-add', 'AdminQuestionController@addNewQuestion');
	Route::post('admin/questions/{id}', 'AdminQuestionController@addAnswer');
	Route::get('admin/question-edit/{id}', 'AdminQuestionController@editQuestion');
	Route::post('admin/question-edit/{id}', 'AdminQuestionController@editThisQuestion');
	Route::get('admin/question-delete/{id}', 'AdminQuestionController@deleteQuestion');

	Route::get('admin/statistics', 'AdminStatisticsController@indexview');
	Route::post('admin/statistics', 'AdminStatisticsController@index');
	Route::get('admin/chat/{id}/{uid}', 'AdminStatisticsController@userchat');
	Route::post('admin/messages/{id}/{uid}/{suid}/retrieveChatMessages', 'AdminStatisticsController@retrieveChatMessages');

	Route::get('admin/how-does-it-works', 'AdminController@howitworks');
	Route::get('admin/terms-and-conditions', 'AdminController@termsandconditions');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
