<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Aloha\Twilio\TwilioInterface;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use Twilio;
use App\User;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

class FrontUserController extends Controller {


    /*
    *User profile
    *Returns: user, user tags
    */
	public function profile()
	{
        $user = Auth::user();
		$tags = DB::table('proftags')->where('qProfOID', '=', $user->qProfOID)->get();

		return view('profile', compact('user', 'tags'));
	}


    /*
    *Function for editing profile
    *Inputs: user data
    *Returns: user (edited profile)
    */
	public function editprofile(Request $request)
	{
		$user =  Auth::user();

		$validator = Validator::make($request->all(), [
			'name' => 'required|min:3',
			'lastname' => 'required|min:3',
			'email' => 'unique:profiles,qEmail,'.Auth::user()->qProfOID.',qProfOID',
			'emailfriend' => 'sometimes|email',
			'password' => 'sometimes|min:4',
			'confirmpassword' => 'sometimes|same:password',
			'dateofbirth' => 'required|date',
			'gender' => 'required|numeric'
		]);

		if($validator->fails())
		{
			return redirect('profile')
				->withErrors($validator)
				->withInput();
		}

		if($request->file('image') && substr($request->file('image')->getMimeType(), 0, 5) == 'image') {
            $file = $request->file('image');
            $filename = uniqid() . $file->getClientOriginalName();

            if(!file_exists('images/users')) {
                mkdir('images/users/', 0777, true);
            }
            $file->move('images/users', $filename);

            if(!file_exists('images/users/thumbs')) {
                mkdir('images/users/thumbs', 0777, true);
            }
            $thumb = Image::make('images/users/' . $filename);
            $thumb->resize(null, 200, function ($constraint) {
			    $constraint->aspectRatio();
			});
			$thumb->save('images/users/thumbs/' . $filename, 50);

            $user->qPicture = $filename;
        }


        $user->qNameFirst = $request->input('name');
        $user->qNamePrefix = $request->input('nameprefix');
        $user->qNameLast = $request->input('lastname');
        $user->qDateOfBirth = date('Y-m-d', strtotime($request->input('dateofbirth')));
        $user->qEmail = $request->input('email');
        $user->qGender = $request->input('gender');
        $user->qAddress = $request->input('address');
        $user->qZipCode = $request->input('postal_code');
        $user->qLocation = $request->input('latlon');
        $user->qCity = $request->input('location');
        $user->qPhoneNumber = $request->input('phone');
        $user->qURLFacebook = $request->input('facebook');
        $user->qURLTwitter = $request->input('twitter');
        $user->qURLLinkedIn = $request->input('linkedin');
        $user->qURLOther = $request->input('other');
        $user->qEmailFriend = $request->input('emailfriend');
        $user->qDescription = $request->input('description');
        $user->qInterest = $request->input('interest');
        $user->qModifiedAt = date('Y-m-d H:i:s');

       	$user->save();

        if($request->input('phone') != '')
        {
            $checkphone = DB::table('phonecodes')->where('qProfOID', '=', Auth::user()->qProfOID)->first();
            if(!$checkphone)
            {
                $code = rand(100000, 999999);

                DB::table('phonecodes')->insert([
                    'qProfOID' => Auth::user()->qProfOID,
                    'qCode' => $code
                ]);

                //Twilio::message($request->input('phone'), $code);
                Session::flash('phone_message', 'We sent you verification code to your phone.');
            }
        }

        if($request->input('phonecode') != '')
        {
            $checkcode = DB::table('phonecodes')->where('qProfOID', '=', Auth::user()->qProfOID)->where('qCode', '=', $request->input('phonecode'))->first();

            if($checkcode)
            {
                $user->qPhoneVerifiedYN = 1;
                $user->save();

                Session::flash('phone_message', 'You successfully verified your phone number.');
            }
            else
            {
                Session::flash('error_message', 'Your phone verification code is invalid.');
            }
        }

        if(count($request->input('tags')) > 0)
        {
            foreach ($request->input('tags') as $tag) {
                if(strlen($tag) > 3){
                    $basetags = DB::table('envtags')
                        ->whereRaw("LOWER(qTagDesc) LIKE '%".strtolower($tag)."%'")
                        ->first();

                    if(!$basetags)
                    {
                        $tagid = DB::table('envtags')->insertGetId([
                            'qTagDesc' => $tag,
                            'qCreatedAt' => date('Y-m-d H:i:s'),
                            'qModifiedAt' => date('Y-m-d H:i:s'),
                            'qIsActive' => 1
                        ]);
                    } else {
                        $tagid = $basetags->qTagOID;
                    }

                    $check = DB::table('proftags')->where('qTagOID', '=', $tagid)->where('qProfOID', '=', $user->qProfOID)->first();

                    if(!$check)
                    {
                        DB::table('proftags')->insert([
                            'qTagOID' => $tagid,
                            'qProfOID' => $user->qProfOID
                        ]);
                    }

                }
            }
        }

        Session::flash('flash_message', 'You successfully edited profile');

        return redirect('profile');
	}


    /*
    *Remove tag from profile
    *Input: tag id
    */
    public function deletetag(Request $request)
    {
        DB::table('proftags')->where('qTagOID', '=', $request->input('id'))->where('qProfOID', '=', Auth::user()->qProfOID)->delete();
    }


    /*
    *View profile of selected user
    *Input: user id
    *Return: selected user and profile comments
    */
    public function viewProfile($id)
    {

        $user = User::find($id);

        if($user->qProfOID != Auth::user()->qProfOID)
        {
            $timestamp = strtotime(date('Y-m-d H:i:s')) - 60*60;
            $time = date('Y-m-d H:i:s', $timestamp);

            $check = DB::table('profileview')
                ->where('qProfOID', '=', $id)
                ->where('qProfOIDViewedBy', '=', Auth::user()->qProfOID)
                ->where('qCreatedAt', '<', $time)
                ->orderBy('qCreatedAt', 'DESC')
                ->first();

            $secondcheck = DB::table('profileview')
                ->where('qProfOID', '=', $id)
                ->where('qProfOIDViewedBy', '=', Auth::user()->qProfOID)
                ->first();

            if($check || !$secondcheck)
            {
                DB::table('profileview')
                    ->insert([
                        'qProfOID' => $id,
                        'qProfOIDViewedBy' => Auth::user()->qProfOID,
                        'qCreatedAt' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        $comment = DB::table('activcomments')
            ->where('qProfOIDActivCreated', '=', $user->qProfOID)
            ->select(DB::raw("qActivOID as whatiscommented, 1 as tablename, qActivCommentsNID as commentedby, qCreatedAt as qCreatedAt, qComment as qComment"));

        $comments = DB::table('profcomments')
            ->where('qProfOID', '=', $user->qProfOID)
            ->select(DB::raw("qProfOID as whatiscommented, 2 as tablename, qProfOIDCreated as commentedby, qCreatedAt as qCreatedAt, qComment as qComment"))
            ->unionall($comment)
            ->get();

        if(!$user)
        {
            return redirect('activities');
        }

        return view('profile-view', compact('user', 'comments'));
    }


    /*
    *Profiles list
    *Input: search form data
    *Returns profiles
    */
    public function profiles()
    {
        $conditiondistance = '';
        $conditionio = '';
        $locationforbase = '';

        if(Input::has('male') && Input::has('female'))
            {
                /*
                *Condition for searching man and woman
                */
                $conditionio = "AND (profiles.qGender=0 OR profiles.qGender=1 OR profiles.qGender=0)";
            }
            else if(Input::has('male') || Input::has('female'))
            {
                if(Input::has('male'))
                {
                    /*
                    *Condition for searching man
                    */
                    $conditionio = "AND profiles.qGender=1";
                } elseif(Input::has('female'))
                {
                    /*
                    *Condition for searching female
                    */
                    $conditionio = "AND profiles.qGender=2";
                }

        }

        if(Input::get('latlon'))
        {
            $coords = explode(', ', Input::get('latlon'));

            $locationforbase = Input::get('latlon');

            /*
            *Convert radius to km
            */
            $km = 1.6093 * Input::get('distance');

            /*
            *Radius condition for searching near activities
            */
            $conditiondistance = "AND ( 3959 * acos( cos( radians(".$coords[0].") ) * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(profiles.qLocation, ', ', 1), ', ', -1) ) ) * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(profiles.qLocation,' | ',1), ', ', -1) ) - radians(".$coords[1].") ) + sin( radians(".$coords[0].") ) * sin( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(profiles.qLocation, ', ', 1), ', ', -1) ) ) ) ) <='".$km."'";


            }

        if(Input::has('tags'))
        {
            $conditiontags = '';
            $tagsforbase = '';

            foreach (Input::get('tags') as $tag) {
                if($tag != '')
                {
                    if($conditiontags == '')
                    {
                        $tagsforbase = '['.$tag.']';
                        $conditiontags = "envtags.qTagOID='".$tag."'";
                    } else {
                        $tagsforbase = $tagsforbase.'['.$tag.']';
                        $conditiontags = $conditiontags." OR envtags.qTagOID='".$tag."'";
                    }
                }
            }

            if($conditiontags != '')
            {
                $conditiontags = "(".$conditiontags.")";
            }


            $profiles = DB::table('profiles')
                ->where('profiles.qIsAdmin', '=', 0)
                ->where('profiles.qIsActive', '=', 0)
                ->where('profiles.qIsDeleted', '=', 0)
                ->where('profiles.qProfOID', '<>', Auth::user()->qProfOID)
                ->join('proftags', 'proftags.qProfOID', '=', 'profiles.qProfOID')
                ->join('envtags', 'envtags.qTagOID', '=', 'proftags.qTagOID')
                ->whereRaw($conditiontags.' '.$conditiondistance.' '.$conditionio)
                ->groupBy('profiles.qProfOID')
                ->orderBy('profiles.qNameFirst', 'ASC')
                ->orderBy('profiles.qNameLast', 'ASC')
                ->paginate(20);

            if(!Input::has('page') && URL::previous() == url('profiles-search'))
            {
                $check = DB::table('searchprof')
                    ->where('qLocation', '=', $locationforbase)
                    ->where('qDistance', '=', Input::get('distance', 0))
                    ->where('qIsMale', '=', Input::get('male', 0))
                    ->where('qIsFemale', '=', Input::get('female', 0))
                    ->where('qTags', '=', $tagsforbase)
                    ->first();

                if($check)
                {
                    DB::table('searchprof')
                        ->where('qSearchOID', '=', $check->qSearchOID)
                        ->update(['qCounter'=>($check->qCounter+1)]);
                }
                else{
                    DB::table('searchprof')
                        ->insert([
                            'qLocation' => $locationforbase,
                            'qDistance' => Input::get('distance', 0),
                            'qIsMale'   => Input::get('male', 0),
                            'qIsFemale' => Input::get('female', 0),
                            'qProfOID'  => Auth::user()->qProfOID,
                            'qTags'     => $tagsforbase,
                            'qCounter'  => 1
                        ]);
                }

            }

            return view('profiles-all', compact('profiles'));
        }


        $profiles = DB::table('profiles')
            ->where('profiles.qIsAdmin', '=', 0)
            ->where('profiles.qIsDeleted', '=', 0)
            ->where('profiles.qProfOID', '<>', Auth::user()->qProfOID)
            ->whereRaw("profiles.qIsActive=0 {$conditiondistance} {$conditionio}")
            ->orderBy('profiles.qNameFirst', 'ASC')
            ->orderBy('profiles.qNameLast', 'ASC')
            ->paginate(20);

        return view('profiles-all', compact('profiles'));
    }


    /*
    *Settings page
    *Return user settings
    */
    public function settings()
    {
        $user = User::find(Auth::user()->qProfOID);

        if(!$user)
        {
            Auth::logout();
        }

        return view('settings', compact('user'));
    }


    /*
    *Change email notification
    *Input: notification checkbox
    *Returns settings with edited data
    */
    public function notification(Request $request)
    {
        if($request->has('notification'))
        {
            $notification = 0;

            Session::flash('flash_message', 'You enabled email notifications.');
        } else {
            $notification = 1;
            Session::flash('flash_message', 'You disabled email notifications.');
        }

        DB::table('profiles')
            ->where('qProfOID', '=', Auth::user()->qProfOID)
            ->update([
                'qNotification' => $notification
            ]);

        return redirect()->back();
    }


    /*
    *Change email for profile
    *Input: email address
    *Returns edited profile data
    */
    public function changeemail(Request $request)
    {
        $user = User::find(Auth::user()->qProfOID);

        $validator = Validator::make($request->all(), [
            'email' => 'unique:profiles,qEmail,'.$user->qProfOID.',qProfOID'
        ]);

        if($validator->fails())
        {
            return redirect('settings')
                ->withErrors($validator);
        }

        $user->qEmail = $request->input('email');

        $user->save();

        Session::flash('flash_message', 'Email is successfully changed.');

        return redirect('settings');
    }


    /*
    *Change password for profile
    *Input: old password, new password, confirmation of new password
    *Return: profile with new password
    *Send email to user if password is changed
    */
    public function changepassword(Request $request)
    {
        $user = User::find(Auth::user()->qProfOID);

        $validator = Validator::make($request->all(), [
            'oldpassword' => 'required',
            'password' => 'required|min:4',
            'confirmpassword' => 'required|same:password',
        ]);

        if($validator->fails())
        {
            return redirect('settings')
                ->withErrors($validator);
        }

        if(Hash::check($request->input('oldpassword'), $user->qPassword))
        {
            $user->qPassword = Hash::make($request->input('password'));
            $user->save();

            Session::flash('flash_message', 'Password is successfully changed.');

            Mail::send('emails.newpassword', ['password' => $request->input('password')], function($message) use ($request, $user)
            {
                $message->from('qare-project@qare.com', 'Qare project');
                $message->to($user->qEmail, $user->qNameFirst.' '.$user->qNameLast)->subject('New password - Qare project');
            });


            return redirect('settings');
        }
        else {

            Session::flash('error_message', 'Invalid password.');

            return redirect('settings');
        }

    }


    /*
    *Delete profile
    *Return: qIsDeleted = 1 for logged profile
    *Logout user
    */
    public function deleteprofile()
    {
        $user = User::find(Auth::user()->qProfOID);

        if($user)
        {
            $user->qIsDeleted = 1;

            $user->save();

            Auth::logout();

            Session::flash('error_message', 'Your account has been removed.');

            return redirect('login');
        }
        else{

            Auth::logout();

            return redirect('login');
        }
    }


    /*
    *View for search profiles
    *Return: all available tags
    */
    public function search()
    {
        $tags = DB::table('proftags')->groupBy('qTagOID')->get();

        return view('profiles-search', compact('tags'));
    }


    public function favourite($id, Request $request)
    {
        $check = DB::table('proffavorites')
            ->where('qProfOID', '=', Auth::user()->qProfOID)
            ->where('qProfFavoritesNID', '=', $id)
            ->where('qOIDFavorite', '=', $id)
            ->where('qTypeOfFavorite', '=', 0)
            ->first();

        if($check)
        {
            return redirect()->back();
        }

        DB::table('proffavorites')
            ->insert([
                'qProfOID' => Auth::user()->qProfOID,
                'qProfFavoritesNID' => $id,
                'qTypeOfFavorite' => 0,
                'qCreatedAt' => date('Y-m-d H:i:s'),
                'qModifiedAt' => date('Y-m-d H:i:s'),
                'qOIDFavorite' => $id
            ]);

        Session::flash('flash_message', 'You successfully added this user to favorite.');

        return redirect()->back();
    }


    /*
    *Favorite profiles view
    *Return: all profiles which are in favorite list for logged user
    */
    public function favorites()
    {

        if( Input::has('name') ) {

            $name = Input::get('name');

            $profiles = DB::table('profiles')
            ->join('proffavorites', 'proffavorites.qOIDFavorite', '=', 'profiles.qProfOID')
            ->where('proffavorites.qProfOID', '=', Auth::user()->qProfOID)
            ->where('qNameFirst', 'LIKE','%'.$name.'%')
            ->select('profiles.*')
            ->paginate(20);
            
        } else {

        $profiles = DB::table('profiles')
            ->join('proffavorites', 'proffavorites.qOIDFavorite', '=', 'profiles.qProfOID')
            ->where('proffavorites.qProfOID', '=', Auth::user()->qProfOID)
            ->select('profiles.*')
            ->paginate(20);
        }

        return view('favorite', compact('profiles'));

    }

    /**
     * Search thru favorites
     *
     *@param 
     *@return void
     */
    public function searchFavorites() 
    {
        if( Input::has('name') ) {
            $name = Input::get('name');
        } else {
            return redirect('/favorites');
        }
    }

    /*
    *Function for remove favorite profile from favorite list
    *Input: profile id
    *Return: delete relation from database
    */
    public function removefavorite($id)
    {
        $check = DB::table('proffavorites')
            ->where('qProfOID', '=', Auth::user()->qProfOID)
            ->where('qOIDFavorite', '=', $id)
            ->first();

        if($check){
            DB::table('proffavorites')
                ->where('qProfOID', '=', Auth::user()->qProfOID)
                ->where('qOIDFavorite', '=', $id)
                ->delete();

            Session::flash('flash_message', 'You successfully remove profile from favorites.');
        }
        else {
            Session::flash('error_message', 'Something is wrong. Please try again.');
        }

        return redirect()->back();
    }

    /*
    *Add comment to user profile
    *Input: comment
    *Retuen: new comment on profile
    */
    public function addcomment($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|min:3'
        ]);

        if($validator->fails())
        {
            return redirect('view-profile/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $check = DB::table('profcomments')
                ->where('qProfOID', '=', $id)
                ->where('qProfCommentsNID', '=', Auth::user()->qProfOID)
                ->where('qProfOIDCreated', '=', Auth::user()->qProfOID)
                ->first();

        if($check)
        {
            Session::flash('flash_message', 'You already commented on this profile.');

            return redirect('view-profile/'.$id);
        }

        DB::table('profcomments')->insert([
            'qProfOID' => $id,
            'qProfCommentsNID' => Auth::user()->qProfOID,
            'qProfOIDCreated' => Auth::user()->qProfOID,
            'qComment' => $request->input('message'),
            'qCreatedAt' => date('Y-m-d H:i:s'),
            'qModifiedAt' => date('Y-m-d H:i:s')
        ]);

        Session::flash('flash_message', 'You successfully commented on this profile.');

        return redirect('view-profile/'.$id);
    }

    /**
     *
     *
     *@param 
     *@return void
     */
    public function makeActivFav(Request $request) 
    {
        $user_id= $request->user;
        $activity_id= $request->activity;

        DB::table('activfavorites')->insert(
            ['profile_id' => $user_id, 'activity_id' => $activity_id]
        );


    }

}
