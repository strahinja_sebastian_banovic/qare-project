<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller {


	public function login()
	{

        if(Auth::user()){
            if(Auth::user()->qIsAdmin == 1)
            {
                return redirect('admin/users');
            }
        }

		return view('admin/login');
	}

	public function loginme(Request $request)
	{
		$validator = Validator::make($request->all(), [
    		'email' => 'required|email',
    		'password' => 'required',
    	]);

    	if($validator->fails())
    	{
    		return redirect('admin/login')
    			->withErrors($validator)
    			->withInput();
    	}

        if($request->input('remember') == 1){ $remember = 1;} else {$remember = null;}

    	if (Auth::attempt([
    			'qEmail' => $request->input('email'),
    			'password' => $request->input('password'),
    			'qIsAdmin' => 1,
                'qIsDeleted' => 0
    	], $remember))
    	{
    		return redirect('admin/users');

    	} else {
    		Session::flash('error_message', 'Wrong credentials');

    		return redirect('admin/login')->withInput();

    	}

	}

    public function logout()
    {
        Auth::logout();

        return redirect('admin/login');
    }


    public function howitworks()
    {

        return view('admin/how-it-works');
    }

    public function termsandconditions()
    {

        return view('admin/terms-and-conditions');
    }

}
