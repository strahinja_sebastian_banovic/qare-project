<?php namespace App\Http\Controllers;
use Auth;
class WelcomeController extends Controller {

	/*
	*First page for logged in user
	*/
	public function index()
	{
		if(Auth::user() && Auth::user()->qIsAdmin == 0)
		{
			return redirect('dashboard');
		}

		return view('welcome');
	}

}
