<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use App\AdminBookings;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class AdminBookingsController extends Controller {

	public function index()
	{

		if(Input::get('activity') || Input::get('user'))
		{
			$bookings = DB::table('activbookings')
	            ->join('profiles', 'profiles.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
	            ->whereRaw("CONCAT (LOWER(profiles.qNameFirst), ' ', LOWER(profiles.qNameLast)) LIKE '%".strtolower(Input::get('user'))."%'")
	            ->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
	            ->whereRaw("LOWER(activities.qTitle) LIKE '%".strtolower(Input::get('activity'))."%'")
	            ->select('activbookings.*')
	            ->orderBy('activbookings.qCreatedAt', 'DESC')
	            ->paginate(50);
		}
		else{
			$bookings = DB::table('activbookings')->orderBy('qCreatedAt', 'DESC')->paginate(50);
		}

		return view('admin.bookings', compact('bookings'));
	}

	public function addBooking()
	{
		$users = DB::table('profiles')->where('qIsAdmin', '=', 0)->where('qIsDeleted', '=', 0)->get();
		$activities = DB::table('activities')->get();

		return view('admin.bookings-add', compact('users', 'activities'));
	}

	public function addNewBooking(Request $request)
	{
		$booking = new AdminBookings;

		$validator = Validator::make($request->all(), [
			'activity' => 'required|numeric',
			'profile' => 'required|numeric',
			'state' => 'required|numeric'
		]);

		if($validator->fails())
		{
			return redirect('admin/booking-add')
				->withErrors($validator)
				->withInput();
		}

		$check = DB::table('activbookings')
			->where('qActivOID', '=', $request->input('activity'))
			->where('qProfOIDBookingCreated', '=', $request->input('profile'))->first();

		if($check)
		{
			Session::flash('error_message', 'User already booked for this activity');
			return redirect('admin/booking-add');
		}

		$booking->qActivOID = $request->input('activity');
		$booking->qProfOIDBookingCreated = $request->input('profile');
		$booking->qStatus = $request->input('state');
		$booking->qModifiedAt = date('Y-m-d H:i:s');
		$booking->qCreatedAt = date('Y-m-d H:i:s');

		$booking->save();

		Session::flash('flash_message', 'You successfully booked activity for user');

        return redirect('admin/bookings');

	}

	public function editBooking($id, $uid)
	{
		$users = DB::table('profiles')->where('qIsAdmin', '=', 0)->where('qIsDeleted', '=', 0)->get();
		$activities = DB::table('activities')->get();
		$booking = AdminBookings::where('qActivOID' , '=', $id)->where('qProfOIDBookingCreated' , '=', $uid)->first();

		return view('admin.bookings-edit', compact('users', 'activities', 'booking'));
	}


	public function editThisBooking($id, $uid, Request $request)
	{
		$booking = DB::table('activbookings')->where('qActivOID' , '=', $id)->where('qProfOIDBookingCreated' , '=', $uid)->first();

		$validator = Validator::make($request->all(), [
			'activity' => 'required|numeric',
			'profile' => 'required|numeric',
			'state' => 'required|numeric'
		]);

		if($validator->fails())
		{
			return redirect('admin/booking-edit/'.$id.'/'.$uid)
				->withErrors($validator)
				->withInput();
		}

		$check = DB::table('activbookings')
			->where('qActivOID', '=', $request->input('activity'))
			->where('qProfOIDBookingCreated', '=', $request->input('profile'))
			->where('qActivOID', '<>', $id)
			->where('qProfOIDBookingCreated', '<>', $uid)->first();

		if($check)
		{
			Session::flash('error_message', 'User already booked for this activity');
			return redirect('admin/booking-edit/'.$id.'/'.$uid);
		}

		$booking = DB::table('activbookings')->update([
			'qActivOID' => $request->input('activity'),
			'qProfOIDBookingCreated' => $request->input('profile'),
			'qStatus' => $request->input('state'),
			'qModifiedAt' => date('Y-m-d H:i:s')
		]);


		Session::flash('flash_message', 'You successfully booked activity for user');

        return redirect('admin/bookings');

	}

	public function deleteBooking($id, $uid)
	{
		$booking = DB::table('activbookings')->where('qActivOID' , '=', $id)->where('qProfOIDBookingCreated' , '=', $uid)->delete();

		Session::flash('flash_message', 'Booking has been successfully deleted');

		return redirect('admin/bookings');
	}


}
