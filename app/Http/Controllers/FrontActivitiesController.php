<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use App\AdminBookings;
use App\AdminActivity;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;

class FrontActivitiesController extends Controller {


    /*
    *All activities wich are not expired
    *Input: if user search for activities - form data
    *Return all activities or filtered activities
    */
	public function index()
	{

        if((Input::has('date') && Input::get('date') != '') || Input::has('public') || Input::has('home') || Input::has('inside') || Input::has('outside') || Input::has('location') || Input::has('distance') || Input::has('days') || Input::has('tags'))
        {

            $conditiondate = '';
            $conditionhp = '';
            $conditionio = '';
            $conditiondayofweek = '';
            $conditiontags = '';
            $tagsforbase = '';
            $locationforbase = '';
            $daysforbase = '';
            $conditiondistance = '';

            if(Input::get('latlon'))
            {
                $coords = explode(', ', Input::get('latlon'));

                /*
                *Convert radius to km
                */
                $km = 1.6093 * Input::get('distance');

                /*
                *Radius condition for searching near activities
                */
                $conditiondistance = "AND ( 3959 * acos( cos( radians(".$coords[0].") ) * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(activities.qLocation, ', ', 1), ', ', -1) ) ) * cos( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(activities.qLocation,' | ',1), ', ', -1) ) - radians(".$coords[1].") ) + sin( radians(".$coords[0].") ) * sin( radians( SUBSTRING_INDEX(SUBSTRING_INDEX(activities.qLocation, ', ', 1), ', ', -1) ) ) ) ) <='".$km."'";

                $locationforbase = Input::get('latlon');
            }

            if(Input::has('date') && Input::get('date') != '')
            {
                /*
                *Condition for search activities with specific date
                */
                $conditiondate = "AND activities.qDate='".date('Y-m-d', strtotime(Input::get('date')))."'";
            }

            if(Input::has('home') && Input::has('public'))
            {
                /*
                *Condition for search activities with public or home flag
                */
                $conditionhp = "AND (activities.qPublicYN=0 OR activities.qPublicYN=1)";
            }
            else if(Input::has('home') || Input::has('public'))
            {
                if(Input::has('home'))
                {
                    /*
                    *Condition for search activities with home flag
                    */
                    $conditionhp = "AND activities.qPublicYN=0";
                } elseif(Input::has('public')) {

                    /*
                    *Condition for search activities with public flag
                    */
                    $conditionhp = "AND activities.qPublicYN=1";
                }

            }

            if(Input::has('inside') && Input::has('outside'))
            {
                /*
                *Condition for search activities with inside or outside flag
                */
                $conditionio = "AND (activities.qOutsideYN=0 OR activities.qOutsideYN=1)";
            }
            else if(Input::has('inside') || Input::has('outside'))
            {
                if(Input::has('inside'))
                {
                    /*
                    *Condition for search activities with inside flag
                    */
                    $conditionio = "AND activities.qPublicYN=0";
                } elseif(Input::has('outside'))
                {
                    /*
                    *Condition for search activities with outside flag
                    */
                    $conditionio = "AND activities.qPublicYN=1";
                }

            }


            /*
            *Condition for search activities with specific weekday(s)
            */
            if(Input::has('days'))
            {
                foreach (Input::get('days') as $day) {
                    if($day != '')
                    {
                        if($conditiondayofweek == ""){
                            $daysforbase = '['.$day.']';
                            $conditiondayofweek = "DAYOFWEEK(activities.qDate)='".$day."'";
                        } else {
                            $daysforbase = $daysforbase.'['.$day.']';
                            $conditiondayofweek = $conditiondayofweek." OR DAYOFWEEK(activities.qDate)='".$day."'";
                        }
                    }
                }

                $conditiondayofweek = 'AND ('.$conditiondayofweek.')';
            }
            else if(Input::get('date') != '' && (bool)strtotime(Input::get('date'))==1)
            {
                $dayofweek = date('w', strtotime(Input::get('date')));
                if($dayofweek == 0)
                {
                    $dayofweek = 7;
                } else {
                    $dayofweek = $dayofweek+1;
                }

                $daysforbase = '['.$dayofweek.']';
            }


            $condition = $conditiondate.' '.$conditionhp.' '.$conditionio.' '.$conditiondayofweek.' '.$conditiondistance;


            /*
            *Condition for search activities with specific tags
            */
            if(Input::has('tags'))
            {
                foreach (Input::get('tags') as $tag) {
                    if($tag != '')
                    {
                        if($conditiontags == '')
                        {
                            $tagsforbase = '['.$tag.']';
                            $conditiontags = "envtags.qTagOID='".$tag."'";
                        } else {
                            $tagsforbase = $tagsforbase.'['.$tag.']';
                            $conditiontags = $conditiontags." OR envtags.qTagOID='".$tag."'";
                        }
                    }

                }

                $activities = DB::table('activities')
                    ->whereRaw("activities.qIsActive=0 AND CONCAT('activities.qDate', ' ', 'activities.qTime')>='".date('Y-m-d H:i:s')."' {$condition}")
                    ->join('activtags', 'activtags.qActivOID', '=', 'activities.qActivOID')
                    ->join('envtags', 'envtags.qTagOID', '=', 'activtags.qTagOID')
                    ->whereRaw($conditiontags)
                    ->groupBy('activities.qActivOID')
                    ->paginate(20);

            }
            else {
                $activities = DB::table('activities')
                    ->whereRaw("activities.qIsActive=0 AND CONCAT('activities.qDate', ' ', 'activities.qTime')>='".date('Y-m-d H:i:s')."' {$condition}")
                    ->groupBy('activities.qActivOID')
                    ->paginate(20);
            }

            if(Input::get('date')=='')
            {
                $date = '0000-00-00';
            } else {
                $date = date('Y-m-d', strtotime(Input::get('date')));
            }

            $check = DB::table('searchactiv')
                ->where('qLocation', '=', $locationforbase)
                ->where('qDistance', '=', Input::get('qDistance', 0))
                ->where('qTags', '=', $tagsforbase)
                ->where('qIsHome', '=', Input::get('home', 0))
                ->where('qIsPublic', '=', Input::get('public', 0))
                ->where('qIsInside', '=', Input::get('inside', 0))
                ->where('qIsOutside', '=', Input::get('outside', 0))
                ->where('qDate', '=', $date)
                ->where('qDays', '=', $daysforbase)
                ->first();

            if($check)
            {
                DB::table('searchactiv')
                    ->where('qSearchOID', '=', $check->qSearchOID)
                    ->update(['qCounter'=>($check->qCounter+1)]);
            }
            else
            {
                DB::table('searchactiv')
                    ->insert([
                        'qLocation' => $locationforbase,
                        'qDistance' => Input::get('qDistance', 0),
                        'qTags'     => $tagsforbase,
                        'qIsHome'   => Input::get('home', 0),
                        'qIsPublic' => Input::get('public', 0),
                        'qIsInside' => Input::get('inside', 0),
                        'qIsOutside'=> Input::get('outside', 0),
                        'qDate'     => $date,
                        'qDays'     => $daysforbase,
                        'qProfOID'  => Auth::user()->qProfOID,
                        'qCounter'  => 1
                    ]);
            }

        }
        else {

            $activities = DB::table('activities')
                ->whereRaw("qIsActive=0 AND CONCAT('qDate', ' ', 'qTime')>='".date('Y-m-d H:i:s')."'")
                ->orderBy('qDate', 'DESC')
                ->orderBy('qTime', 'DESC')
                ->paginate(20);
        }

        $page = 'activities';

        return view('activities', compact('activities', 'page'));
	}


    /*
    *My activities
    */
	public function myactivities()
	{

		if(Input::get('date'))
		{
			$condition = "qDate='".date('Y-m-d', strtotime(Input::get('date')))."'";
		} else {
			$condition = "CONCAT('qDate', ' ', 'qTime')>='".date('Y-m-d H:i').":00'";
		}

		$activities = DB::table('activities')
		->whereRaw($condition)
		->where('qProfOIDCreated', '=', Auth::user()->qProfOID)
		->where('qIsActive', '=', 0)
		->orderBy('qDate', 'DESC')
		->paginate(20);

		$page = 'myactivities';

		return view('activities', compact('activities', 'page'));
	}


    /*
    *Single activity
    *Input: activity id
    *Return single activity
    */
	public function singleactivity($id)
	{

		$activity = DB::table('activities')
		->where('qActivOID', '=', $id)
		->where('qIsActive', '=', 0)
		->first();

        if(!$activity)
        {
            return redirect('activities');
        }

		$nofbooking = DB::table('activbookings')
		->where('qActivOID', '=', $activity->qActivOID)
		->where('qStatus', '=', 1)
		->count();

		$tags = DB::table('activtags')
		->where('qActivOID', '=', $activity->qActivOID)
		->get();

		return view('activity-single', compact('activity', 'nofbooking', 'tags'));
	}

    /*
    *Book activity
    *Input: activity id
    *Return: new booking request
    */
	public function bookactivity($id)
	{
		$activity = DB::table('activities')->where('qActivOID', '=', $id)->first();

		if($activity->qProfOIDCreated == Auth::user()->qProfOID)
		{
			Session::flash('error_message', "You can't book your own activity.");

			return redirect('activities/'.$activity->qActivOID);
		}

		$checkifactofuser = DB::table('activbookings')
		->where('qActivOID', '=', $activity->qActivOID)
		->where('qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
		->first();

		if($checkifactofuser)
		{
			Session::flash('error_message', "You already booked this activity.");

			return redirect('activities/'.$activity->qActivOID);
		}

		$checkifspacesavailable = DB::table('activbookings')
		->where('qActivOID', '=', $activity->qActivOID)
		->where('qStatus', '=', 1)
		->count();

		if($activity->qNofSpaces - $checkifspacesavailable == 0)
		{
			Session::flash('error_message', "There are no free space for this activity.");

			return redirect('activities/'.$activity->qActivOID);
		}

		$activbooking = new AdminBookings;

		$activbooking->qActivOID = $id;
		$activbooking->qProfOIDBookingCreated = Auth::user()->qProfOID;
		$activbooking->qCreatedAt = date('Y-m-d H:i:s');
		$activbooking->qModifiedAt = date('Y-m-d H:i:s');

		$activbooking->save();

		Session::flash('flash_message', 'You successfully booked activity.');

		return redirect('invites');

	}


    /*
    *Form for add a new activity
    */
	public function addactivity()
	{
		return view('activities-add');
	}


    /*
    *Add new activity to database
    *Input: form data
    *Return: new activity
    */
	public function addNewActivity(Request $request)
	{
		$activity = new AdminActivity;

        $messages = [
            'postal_code.required' => 'Something is wrong with your location. Please enter it again.'
        ];

		$validator = Validator::make($request->all(), [
			'title' => 'required|min:3',
			'description' => 'required|min:3',
			'date' => 'required|date',
			'time' => 'required|date_format: H:i',
			'spaces' => 'required|numeric',
			'duration' => 'required|numeric',
			'price' => 'required|numeric',
            'postal_code' => 'required'
			], $messages);

        if(Input::get('city') == '' || Input::get('latlon') == '')
        {
            Session::flash('error_message', 'Something is wrong with your location. Please enter it again.');

            return redirect()->back()->withInput();
        }


		if($validator->fails())
		{
			return redirect('add-activity')
			->withErrors($validator)
			->withInput();
		}

		$activity->qProfOIDCreated = Auth::user()->qProfOID;
		$activity->qTitle = $request->input('title');
		$activity->qDescription = $request->input('description');
        $activity->qLocation = $request->input('latlon').' | '.$request->input('postal_code').', '.$request->input('city');

		$activity->qDate = date('Y-m-d', strtotime($request->input('date')));
		$activity->qTime = date('H:i:s', strtotime($request->input('time')));


        if($request->input('public') == 1)
        {
        	$activity->qPublicYN = 1;
        } else {
        	$activity->qPublicYN = 0;
        }

        if($request->input('outside') == 1)
        {
        	$activity->qOutsideYN = 1;
        } else {
        	$activity->qOutsideYN = 0;
        }

        $activity->qNofSpaces = $request->input('spaces');
        $activity->qURLLocation = $request->input('locationurl');
        $activity->qDuration = $request->input('duration');
        $activity->qPrice = $request->input('price');
        $activity->qCreatedAt = date('Y-m-d H:i:s');
        $activity->qModifiedAt = date('Y-m-d H:i:s');

        $activity->save();


        if(count($request->input('tags')) > 0)
        {
        	foreach ($request->input('tags') as $tag) {
        		if(strlen($tag) > 3){
        			$basetags = DB::table('envtags')
        			->whereRaw("LOWER(qTagDesc) LIKE '%".strtolower($tag)."%'")
        			->first();

        			if(!$basetags)
        			{
        				$tagid = DB::table('envtags')->insertGetId([
        					'qTagDesc' => $tag,
        					'qCreatedAt' => date('Y-m-d H:i:s'),
        					'qModifiedAt' => date('Y-m-d H:i:s'),
        					'qIsActive' => 1
        					]);
        			} else {
        				$tagid = $basetags->qTagOID;
        			}

        			$check = DB::table('activtags')->where('qTagOID', '=', $tagid)->where('qActivOID', '=', $activity->qActivOID)->first();

        			if(!$check)
        			{
        				DB::table('activtags')->insert([
        					'qTagOID' => $tagid,
        					'qActivOID' => $activity->qActivOID
        					]);
        			}

        		}
        	}
        }

        Session::flash('flash_message', 'Activity is successfully added');

        return redirect('activities');

    }


    /*
    *Booking invites
    *Return all bookings
    */
    public function invites()
    {
    	$user = User::find(Auth::user()->qProfOID);


    	$myinvites = DB::table('activbookings')->select('activbookings.qActivOID', 'activbookings.qProfOIDBookingCreated', 'activbookings.qStatus', 'activbookings.qCreatedAt')
    					->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
    					->join('profiles', 'activities.qProfOIDCreated', '=', 'profiles.qProfOID')
    					->where('profiles.qProfOID', '=', $user->qProfOID)
    					->orderBy('qStatus', 'ASC');

    	$invites = DB::table('activbookings')->select('qActivOID', 'qProfOIDBookingCreated', 'qStatus', 'qCreatedAt')
    		->where('activbookings.qProfOIDBookingCreated', '=', $user->qProfOID)
    		->union($myinvites)
    		->orderBy('qStatus', 'ASC')
    		->get();

    	$page = Input::get('page', 1);
    	$perPage = 20;
    	$allinvites = array_slice($invites, $perPage * ($page - 1), $perPage);


    	$allinvites = new LengthAwarePaginator($allinvites, count($invites), $perPage, Paginator::resolveCurrentPage(), ['path' => Paginator::resolveCurrentPath()]);

    	return view('invites', compact('user', 'allinvites'));
    }


    /*
    *Single invite view
    *Input: activity id, user id
    *Return single invite
    */
    public function invitesView($aid, $bid)
    {
    	$activity = DB::table('activities')->where('qActivOID', '=', $aid)->first();

        if(!$activity)
        {
            return redirect('invites');
        }

    	$activityowner = User::find($activity->qProfOIDCreated);

    	$booking = DB::table('activbookings')->where('qActivOID', '=', $aid)->where('qProfOIDBookingCreated', '=', $bid)->first();
    	$bookinguser = User::find($booking->qProfOIDBookingCreated);

    	if($activityowner->qProfOID != Auth::user()->qProfOID && $bookinguser->qProfOID != Auth::user()->qProfOID)
    	{
    		return redirect('invites');
    	}

    	return view('invites-single', compact('activity', 'activityowner', 'booking', 'bookinguser'));

    }


    /*
    *Accept or refuse booking
    *Input: my activity id, user id, action (accept/refuse)
    *Return: new booking status
    */
    public function invitesViewAction($aid, $bid, $action)
    {
    	$check = DB::table('activities')
    		->where('qProfOIDCreated', '=', Auth::user()->qProfOID)
    		->first();

    	if(!$check)
    	{
    		return redirect()->back();
    	}

    	$booking = DB::table('activbookings')
    		->where('qActivOID', '=', $aid)
    		->where('qProfOIDBookingCreated', '=', $bid)
    		->where('qStatus', '=', 0)
    		->first();

    	if(!$booking)
    	{
    		return redirect()->back();
    	}

    	if($action == 'accept')
    	{
    		$value = 1;
    	} else if($action == 'decline')
    	{
    		$value = 2;
    	}

    	DB::table('activbookings')
    		->where('qActivOID', '=', $aid)
    		->where('qProfOIDBookingCreated', '=', $bid)
    		->where('qStatus', '=', 0)
    		->update(['qStatus'=>$value]);

    	if($action == 'accept'){
    		Session::flash('flash_message', 'You accepted this user for activity.');
    	} else if($action == 'decline')
    	{
    		Session::flash('error_message', 'You declined this user for activity.');
    	}

    	return redirect('invites/'.$aid.'/'.$bid);

    }


    /*
    *Search page for activities
    *Return: all available tags
    */
    public function search()
    {
        $tags = DB::table('activtags')->groupBy('qTagOID')->get();

    	return view('activities-search', compact('tags'));
    }


    /*
    *Delete sent request for activity
    *Input: activity id
    *Return: delete request from database
    */
    public function cancelactivity($id)
    {
        $check = DB::table('activbookings')
            ->where('qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
            ->where('qActivOID', '=', $id)
            ->where('qStatus', '=', 0)
            ->first();

        if(!$check)
        {
            return redirect()->back();
        }

        DB::table('activbookings')
            ->where('qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
            ->where('qActivOID', '=', $id)
            ->where('qStatus', '=', 0)
            ->delete();

        Session::flash('flash_message', 'You successfully deleted requested booking.');

        return redirect('invites');
    }

    /*
    *Write comment for profile when activity is finished
    *Input: Activity id, comment
    *Return: new comment on profile page
    */
    public function addcomment($id, Request $request)
    {

        $check = DB::table('activbookings')
            ->where('activbookings.qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
            ->join('activities', function($join)
            {
                $join->on('activities.qActivOID', '=', 'activbookings.qActivOID')
                     ->on(DB::raw("CONCAT(activities.qDate, ' ', activities.qTime)<='".date('Y-m-d H:i:s')."'"), DB::raw(''), DB::raw(''));
            })
            ->where('activities.qProfOIDCreated', '<>', Auth::user()->qProfOID)
            ->where('activbookings.qActivOID', '=', $id)
            ->orderBy('activbookings.qCreatedAt', 'DESC')
            ->select('activbookings.*')
            ->first();

        if(!$check)
        {
            return redirect()->back();
        }

        $activity = DB::table('activities')
            ->where('qActivOID', '=', $check->qActivOID)
            ->first();

        if($activity->qActivOID != $id)
        {
            return redirect()->back();
        }

        $validator = Validator::make($request->all(), [
            'message' => 'required|min:3'
        ]);

        if($validator->fails())
        {
            return redirect()->back()
                ->withErrors($validator);
        }

        $checkifhavecomment = DB::table('activcomments')
            ->where('qActivOID', '=', $activity->qActivOID)
            ->where('qActivCommentsNID', '=', Auth::user()->qProfOID)
            ->where('qProfOIDActivCreated', '=',$activity->qProfOIDCreated)
            ->first();

        if($checkifhavecomment)
        {
            Session::flash('error_message', 'You already commented this activity.');

            return redirect()->back();
        }

        DB::table('activcomments')
            ->insert([
                'qActivOID'=>$activity->qActivOID,
                'qActivCommentsNID'=>Auth::user()->qProfOID,
                'qCreatedAt'=>date('Y-m-d H:i:s'),
                'qModifiedAt'=>date('Y-m-d H:i:s'),
                'qProfOIDActivCreated'=>$activity->qProfOIDCreated,
                'qComment'=>$request->input('message')
            ]);

        Session::flash('flash_message', 'You successfully commented this activity.');

        return redirect()->back();

    }

}
