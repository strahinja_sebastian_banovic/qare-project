<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminUserController extends Controller {


	public function index()
	{
        if(Input::get('user'))
        {
            $users = DB::table('profiles')->where('qIsDeleted', '=', 0)
            ->whereRaw("CONCAT (LOWER(qNameFirst), ' ', LOWER(qNameLast)) LIKE '%".strtolower(Input::get('user'))."%'")
            ->orderBy(DB::raw("CONCAT (LOWER(qNameFirst), ' ', LOWER(qNameLast))", 'ASC'))
            ->paginate(50);
        } else {
            $users = DB::table('profiles')->where('qIsDeleted', '=', 0)->orderBy(DB::raw("CONCAT (LOWER(qNameFirst), ' ', LOWER(qNameLast))", 'ASC'))->paginate(50);
        }


		return view('admin.users', compact('users'));
	}


	public function adduser()
	{
		return view('admin.users-add');
	}

	public function addNewUser(Request $request)
	{

		$user = new User;

		$validator = Validator::make($request->all(), [
			'name' => 'required|min:3',
			'lastname' => 'required|min:3',
			'email' => 'required|unique:profiles,qEmail',
			'password' => 'required|min:4',
			'confirmpassword' => 'required|same:password',
			'emailfriend' => 'sometimes|email',
			'dateofbirth' => 'sometimes|date',
			'gender' => 'required|numeric'
		]);

		if($validator->fails())
		{
			return redirect('admin/user-add')
				->withErrors($validator)
				->withInput();
		}

		if($request->file('image') && substr($request->file('image')->getMimeType(), 0, 5) == 'image') {
            $file = $request->file('image');
            $filename = uniqid() . $file->getClientOriginalName();

            if(!file_exists('images/users')) {
                mkdir('images/users/', 0777, true);
            }
            $file->move('images/users', $filename);

            if(!file_exists('images/users/thumbs')) {
                mkdir('images/users/thumbs', 0777, true);
            }
            $thumb = Image::make('images/users/' . $filename);
            $thumb->resize(null, 200, function ($constraint) {
			    $constraint->aspectRatio();
			});
			$thumb->save('images/users/thumbs/' . $filename, 50);

            $user->qPicture = $filename;
        }

        if($request->input('admin') == 1)
        {
        	$user->qIsAdmin = 1;
        } else {
        	$user->qIsAdmin = 0;
        }

        if($request->input('active') == 1)
        {
        	$user->qIsActive = 0;
        } else {
        	$user->qIsActive = 1;
        }

        if($request->input('malesearch') == 1)
        {
        	$user->qPrefMaleYN = 1;
        } else {
        	$user->qPrefMaleYN = 0;
        }

        if($request->input('femalesearch') == 1)
        {
        	$user->qPrefFemaleYN = 1;
        } else {
        	$user->qPrefFemaleYN = 0;
        }

        if($request->input('insidesearch') == 1)
        {
        	$user->qPrefInsideYN = 1;
        } else {
        	$user->qPrefInsideYN = 0;
        }

        if($request->input('outsidesearch') == 1)
        {
        	$user->qPrefOutsideYN = 1;
        } else {
        	$user->qPrefOutsideYN = 0;
        }

        if($request->input('individualsearch') == 1)
        {
        	$user->qPrefIndividualYN = 1;
        } else {
        	$user->qPrefIndividualYN = 0;
        }

        if($request->input('groupsearch') == 1)
        {
        	$user->qPrefGroupYN = 1;
        } else {
        	$user->qPrefGroupYN = 0;
        }


        if($request->input('admin') != 1)
        {
            $emailhash = Hash::make(uniqid());
            $user->qEmailHash = $emailhash;
            $password = $request->input('password');
        }


        $user->qNameFirst = $request->input('name');
        $user->qNamePrefix = $request->input('nameprefix');
        $user->qNameLast = $request->input('lastname');
        $user->qDateOfBirth = date('Y-m-d', strtotime($request->input('dateofbirth')));
        $user->qEmail = $request->input('email');
        $user->qGender = $request->input('gender');
        $user->qAddress = $request->input('address');
        $user->qZipCode = $request->input('zipcode');
        $user->qLocation = $request->input('latlon');
        $user->qCity = $request->input('city');
        $user->qPhoneNumber = $request->input('phone');
        $user->qURLFacebook = $request->input('facebook');
        $user->qURLTwitter = $request->input('twitter');
        $user->qURLLinkedIn = $request->input('linkedin');
        $user->qURLOther = $request->input('other');
        $user->qEmailFriend = $request->input('emailfriend');
        $user->qProfileMemberLevel = $request->input('profilememberlevel');
        $user->qDescription = $request->input('description');
        $user->qInterest = $request->input('interest');
        if($request->input('distance')){
            $user->qPrefDistance = $request->input('distance');
        } else {
            $user->qPrefDistance = 0;
        }
        $user->qCreatedAt = date('Y-m-d H:i:s');
        $user->qModifiedAt = date('Y-m-d H:i:s');

        $user->qPassword = Hash::make($request->input('password'));
        $user->save();

        $newuser = User::find($user->qProfOID);


        if($request->input('admin') != 1)
        {
            Mail::send('emails.register', compact('newuser', 'emailhash', 'password'), function($message) use ($newuser)
            {
                $message->from('qare-project@qare.com', 'Qare project');
                $message->to($newuser->qEmail, $newuser->qNameFirst.' '.$newuser->qNameLast)->subject('Registration - Qare project');
            });
        }

        Session::flash('flash_message', 'User is successfully added');

        return redirect('admin/users');
	}


	public function useredit($id)
	{
		$user = DB::table('profiles')->where('qProfOID', '=', $id)->first();

		if($user)
		{
			return view('admin.users-edit', compact('user'));
		}

	}

	public function usereditprofile($id, Request $request)
	{
		$user =  User::find($id);

		$validator = Validator::make($request->all(), [
			'name' => 'required|min:3',
			'lastname' => 'required|min:3',
			'email' => 'unique:profiles,qEmail,'.$id.',qProfOID',
			'emailfriend' => 'sometimes|email',
			'password' => 'sometimes|min:4',
			'confirmpassword' => 'sometimes|same:password',
			'dateofbirth' => 'sometimes|date',
			'gender' => 'required|numeric'
		]);

		if($validator->fails())
		{
			return redirect('admin/users-edit/'.$user->qProfOID)
				->withErrors($validator)
				->withInput();
		}

		if($request->file('image') && substr($request->file('image')->getMimeType(), 0, 5) == 'image') {
            $file = $request->file('image');
            $filename = uniqid() . $file->getClientOriginalName();

            if(!file_exists('images/users')) {
                mkdir('images/users/', 0777, true);
            }
            $file->move('images/users', $filename);

            if(!file_exists('images/users/thumbs')) {
                mkdir('images/users/thumbs', 0777, true);
            }
            $thumb = Image::make('images/users/' . $filename);
            $thumb->resize(null, 200, function ($constraint) {
			    $constraint->aspectRatio();
			});
			$thumb->save('images/users/thumbs/' . $filename, 50);

            $user->qPicture = $filename;
        }

        if($request->input('password')!='' && $request->input('confirmpassword')!='')
        {
        	$user->qPassword = Hash::make($request->input('password'));
        }

        if($request->input('admin') == 1)
        {
        	$user->qIsAdmin = 1;
        } else {
        	$user->qIsAdmin = 0;
        }

        if($request->input('active') == 1)
        {
        	$user->qIsActive = 0;
        } else {
        	$user->qIsActive = 1;
        }

        if($request->input('malesearch') == 1)
        {
        	$user->qPrefMaleYN = 1;
        } else {
        	$user->qPrefMaleYN = 0;
        }

        if($request->input('femalesearch') == 1)
        {
        	$user->qPrefFemaleYN = 1;
        } else {
        	$user->qPrefFemaleYN = 0;
        }

        if($request->input('insidesearch') == 1)
        {
        	$user->qPrefInsideYN = 1;
        } else {
        	$user->qPrefInsideYN = 0;
        }

        if($request->input('outsidesearch') == 1)
        {
        	$user->qPrefOutsideYN = 1;
        } else {
        	$user->qPrefOutsideYN = 0;
        }

        if($request->input('individualsearch') == 1)
        {
        	$user->qPrefIndividualYN = 1;
        } else {
        	$user->qPrefIndividualYN = 0;
        }

        if($request->input('groupsearch') == 1)
        {
        	$user->qPrefGroupYN = 1;
        } else {
        	$user->qPrefGroupYN = 0;
        }

        $user->qNameFirst = $request->input('name');
        $user->qNamePrefix = $request->input('nameprefix');
        $user->qNameLast = $request->input('lastname');
        $user->qDateOfBirth = date('Y-m-d', strtotime($request->input('dateofbirth')));
        $user->qEmail = $request->input('email');
        $user->qGender = $request->input('gender');
        $user->qAddress = $request->input('address');
        $user->qZipCode = $request->input('zipcode');
        $user->qLocation = $request->input('latlon');
        $user->qCity = $request->input('city');
        $user->qPhoneNumber = $request->input('phone');
        $user->qURLFacebook = $request->input('facebook');
        $user->qURLTwitter = $request->input('twitter');
        $user->qURLLinkedIn = $request->input('linkedin');
        $user->qURLOther = $request->input('other');
        $user->qEmailFriend = $request->input('emailfriend');
        $user->qProfileMemberLevel = $request->input('profilememberlevel');
        $user->qDescription = $request->input('description');
        $user->qInterest = $request->input('interest');
        if($request->input('distance')){
            $user->qPrefDistance = $request->input('distance');
        } else {
            $user->qPrefDistance = 0;
        }
        $user->qModifiedAt = date('Y-m-d H:i:s');

       	$user->save();

        Session::flash('flash_message', 'You successfully edited profile');

        return redirect('admin/users');
	}

	public function deleteuser($id)
	{
		$user = User::find($id);

        if($user)
        {
            $user->qIsDeleted = 1;
            $user->save();
        }

		Session::flash('flash_message', 'User has been successfully deleted');

		return redirect('admin/users');
	}

    public function sendemail(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'message' => 'required|min:3',
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }

        $messagebody = $request->input('message');

        Mail::send('emails.adminmessagetouser', ['messagebody'=>$messagebody], function($message) use ($request)
        {
            $message->from('qare-project@qare.com', 'Qare project');
            $message->to($request->input('email'))->subject('Qare project - message');
        });

        Session::flash('flash_message', 'Email has been sent.');

        return redirect()->back();
    }

}
