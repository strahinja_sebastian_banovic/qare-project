<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Session;

class FrontChatController extends Controller {


	/*
	*User inbox
	*Return chat
	*/
	public function inbox()
	{
		$user = Auth::user();

		$chat = DB::table('chat')->where('qStatus', '=', 1)
			->where(function($query) use ($user)
            {
                $query->where('qProfOID', '=', $user->qProfOID)
					  ->orWhere('qProfOIDRequested', '=', $user->qProfOID);
            })
			->get();

		return view('messages', compact('chat'));
	}


	/*
	*Send chat request from activity
	*Input chat id, action (accept/refuse)
	*Return: accepted or refused request
	*/
	public function chatrequestactivity($id, Request $request)
	{
		$activity = DB::table('activities')
			->where('qActivOID', '=', $id)
			->first();

		if(!$activity)
		{
			return redirect()->back();
		}

		if($activity->qProfOIDCreated == Auth::user()->qProfOID)
		{
			Session::flash('error_message', "You can't send request to yourself.");

			return redirect('activities/'.$activity->qActivOID);
		}

		$user = User::find($activity->qProfOIDCreated);

		if(!$user)
        {
            Session::flash('error_message', "Error! User doesn't exist.");

            return redirect('activities/'.$activity->qActivOID);
        }

        $check = DB::table('chat')
            ->where('qProfOIDRequested', '=', Auth::user()->qProfOID)
            ->where('qProfOID', '=', $user->qProfOID)
            ->where('qStatus', '<>', 2)
            ->first();

        if($check)
        {
            Session::flash('error_message', "Request already sent.");

            return redirect('activities/'.$activity->qActivOID);
        }

        $checkifgetrequest = DB::table('chat')
            ->where('qProfOID', '=', Auth::user()->qProfOID)
            ->where('qProfOIDRequested', '=', $user->qProfOID)
            ->where('qStatus', '<>', 2)
            ->first();

        if($checkifgetrequest)
        {
            Session::flash('error_message', "This user sent you request.");

            return redirect('activities/'.$activity->qActivOID);
        }

        DB::table('chat')->insertGetId([
            'qProfOIDRequested' => Auth::user()->qProfOID,
            'qProfOID' => $user->qProfOID
        ]);

        Session::flash('flash_message', 'Request has been sent.');

        return redirect('activities/'.$activity->qActivOID);

	}


	/*
	*Send request to user for chat from profile page
	*Input: profile id
	*Return: chat request
	*/
	public function chatrequest($id, Request $request)
    {
        if($id == Auth::user()->qProfOID)
        {
            Session::flash('error_message', "You can't send request to yourself.");

            return redirect('view-profile/'.$id);
        }

        $user = User::find($id);

        if(!$user)
        {
            Session::flash('error_message', "Error! User doesn't exist.");

            return redirect('view-profile/'.$id);
        }

        $check = DB::table('chat')
            ->where('qProfOIDRequested', '=', Auth::user()->qProfOID)
            ->where('qProfOID', '=', $id)
            ->where('qStatus', '<>', 2)
            ->first();

        if($check)
        {
            Session::flash('error_message', "Request already sent.");

            return redirect('view-profile/'.$id);
        }

        $checkifgetrequest = DB::table('chat')
            ->where('qProfOID', '=', Auth::user()->qProfOID)
            ->where('qProfOIDRequested', '=', $id)
            ->where('qStatus', '<>', 2)
            ->first();

        if($checkifgetrequest)
        {
            Session::flash('error_message', "This user sent you request.");

            return redirect('view-profile/'.$id);
        }

        DB::table('chat')->insertGetId([
            'qProfOIDRequested' => Auth::user()->qProfOID,
            'qProfOID' => $id
        ]);

        Session::flash('flash_message', 'Request has been sent.');

        return redirect('view-profile/'.$id);

    }


    /*
	*Accept or refuse requests for chat
	*Input: chat id, action (accept/decline)
	*Return accepted or refused request
    */
    public function chatrequestAcceptOrDecline($id, $action)
    {
    	$check = DB::table('chat')
    		->where('qProfOIDRequested', '=', $id)
    		->where('qProfOID', '=', Auth::user()->qProfOID)
    		->where('qStatus', '=', 0)
    		->first();

    	if(!$check)
    	{
    		return redirect()->back();
    	}

    	if($action == 'accept')
    	{
    		$value = 1;
    	} else if($action == 'decline')
    	{
    		$value = 2;
    	}

    	DB::table('chat')
    		->where('qProfOIDRequested', '=', $id)
    		->where('qProfOID', '=', Auth::user()->qProfOID)
    		->where('qStatus', '=', 0)
    		->update(['qStatus'=>$value]);

    	if($action == 'accept')
    	{
    		Session::flash('flash_message', 'You accepted chat request. You can chat with this user.');
    	} else if($action == 'decline')
    	{
    		Session::flash('error_message', 'You declined chat request.');
    	}

    	return redirect()->back();

    }


    /*
	*
    */
	public function chatWithUser($id)
	{
		$user = User::find($id);

		$check = DB::table('chat')
		->where(function($queryw) use ($user)
            {
                $queryw->where('qProfOIDRequested', '=', $user->qProfOID)
					   ->where('qProfOID', '=', Auth::user()->qProfOID);
            })
		->orWhere(function($query) use ($user)
            {
                $query->where('qProfOID', '=', $user->qProfOID)
					  ->where('qProfOIDRequested', '=', Auth::user()->qProfOID);
            })
		->where('qStatus', '=', 1)
		->first();

		if(!$check)
		{
			Session::flash('error_message', "You are not allowed to chat with this user");

			return redirect('messages');
		}

		DB::table('chatmessages')->where('qChatOID', '=', $check->qChatOID)->where('qProfOIDReceiver', '=', Auth::user()->qProfOID)->update(['qSeen'=>1]);


		$allchat = DB::table('chatmessages')
				->where('qChatOID', '=', $check->qChatOID)
				->get();

		return view('chatwithuser', compact('user', 'allchat'));

	}


	/*
	*Send chat message to user
	*Input: user id, message
	*Return new message
	*/
	public function sendmessage($id, Request $request)
	{
		$user = User::find($id);

		$check = DB::table('chat')
		->where(function($queryw) use ($user)
            {
                $queryw->where('qProfOIDRequested', '=', $user->qProfOID)
					   ->where('qProfOID', '=', Auth::user()->qProfOID);
            })
		->orWhere(function($query) use ($user)
            {
                $query->where('qProfOID', '=', $user->qProfOID)
					  ->where('qProfOIDRequested', '=', Auth::user()->qProfOID);
            })
		->where('qStatus', '=', 1)
		->first();

		if(!$check)
		{
			return redirect('messages');
		}

		if($request->input('message'))
		{
			DB::table('chatmessages')->insert([
				'qChatOID' => $check->qChatOID,
				'qProfOIDSender' => Auth::user()->qProfOID,
				'qProfOIDReceiver' => $user->qProfOID,
				'qMessage' => $request->input('message')
			]);

			return $request->input('message');
		}

	}

	/*
	*Function for pull all new messages for selected user
	*Input: chat id
	*If user see message, message get status seen
	*Return all messages for this chat
	*/
	public function retrieveChatMessages($id, Request $request)
	{
		$user = User::find($id);

		$check = DB::table('chat')
		->where(function($queryw) use ($user)
            {
                $queryw->where('qProfOIDRequested', '=', $user->qProfOID)
					   ->where('qProfOID', '=', Auth::user()->qProfOID);
            })
		->orWhere(function($query) use ($user)
            {
                $query->where('qProfOID', '=', $user->qProfOID)
					  ->where('qProfOIDRequested', '=', Auth::user()->qProfOID);
            })
		->where('qStatus', '=', 1)
		->first();

		if(!$check)
		{
			return redirect('messages');
		}

		$message = DB::table('chatmessages')->where('qSeen', '=', 0)->where('qProfOIDReceiver', '=', Auth::user()->qProfOID)->where('qProfOIDSender', '=', $user->qProfOID)->first();

		if(count($message) > 0)
		{

			DB::table('chatmessages')->where('qSeen', '=', 0)->where('qChatMessageOID', '=', $message->qChatMessageOID)->where('qProfOIDReceiver', '=', Auth::user()->qProfOID)->update(['qSeen' => 1]);

			$sender = User::find($message->qProfOIDSender);

			if($sender->qPicture)
			{
				$image = '<img src="'.url('images/users/thumbs/'.$user->qPicture).'">';
			} else {
				if($sender->qGender==0 || $sender->qGender==1)
				{
					$image = '<img src="'.url('img/user-male.png').'">';
				}
				else
				{
					$image = '<img src="'.url('img/user-female.png').'">';
				}

			}

			$returnhtml = '<div class="row inbox message"><div class="col-md-4 col-xs-2 image">'.$image.'</div><div class="col-md-8 col-xs-10"><p>'.$message->qMessage.'</p></div></div>';

			return $returnhtml;
		}

	}


	/*
	*Function for counting new messages for logged user
	*Return count of new messages
	*/
	public function retrieveCountChatMessages()
	{
		$counter = DB::table('chatmessages')
			->where('qProfOIDReceiver', '=', Auth::user()->qProfOID)
			->where('qSeen', '=', 0)
			->groupBy('qChatOID')
			->get();

		if(count($counter) > 0)
		{
			$returnhtml = '<i class="glyphicon glyphicon-envelope"></i> <span class="badge">'.count($counter).'</span>';
		} else {
			$returnhtml = '<i class="glyphicon glyphicon-envelope"></i>';
		}

		return $returnhtml;
	}


	/*
	*Chat invites for user
	*Return all chat requests
	*/
	public function chatinvites()
	{
		$invites = DB::table('chat')
			->where('qProfOID', '=', Auth::user()->qProfOID)
			->orWhere('qProfOIDRequested', '=', Auth::user()->qProfOID)
			->orderBy('qStatus', 'ASC')
			->paginate(20);

		return view('invites-chat', compact('invites'));
	}


	/*
	*Function for accept or refuse chat request
	*Input: chat id, action (accept/refuse)
	*Return: new chat status
	*/
	public function chatinvitesAction($id, $action)
	{
		$request = DB::table('chat')
			->where('qProfOID', '=', Auth::user()->qProfOID)
			->where('qStatus', '=', 0)
			->where('qChatOID', '=', $id)
			->first();

		if(!$request)
		{
			return redirect()->back();
		}

		if($action == 'accept')
		{
			$value = 1;
		} else if($action == 'decline')
		{
			$value = 2;
		}

		DB::table('chat')
			->where('qProfOID', '=', Auth::user()->qProfOID)
			->where('qStatus', '=', 0)
			->where('qChatOID', '=', $id)
			->update(['qStatus'=>$value]);

		return redirect()->back();

	}

}
