<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use App\AdminActivity;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class AdminActivityController extends Controller {

	public function index()
	{
        if(Input::get('activity') || Input::get('user'))
        {
            $activities = DB::table('activities')
            ->whereRaw("LOWER(activities.qTitle) LIKE '%".strtolower(Input::get('activity'))."%'")
            ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
            ->whereRaw("CONCAT (LOWER(profiles.qNameFirst), ' ', LOWER(profiles.qNameLast)) LIKE '%".strtolower(Input::get('user'))."%'")
            ->select('activities.*')
            ->orderBy('activities.qCreatedAt', 'DESC')
            ->paginate(50);
        }
        else {
            $activities = DB::table('activities')->orderBy('qCreatedAt', 'DESC')->paginate(50);
        }


		return view('admin.activities', compact('activities'));

	}

	public function addActivity()
	{
		$users = DB::table('profiles')->where('qIsAdmin', '=', 0)->get();

		return view('admin.activity-add', compact('users'));
	}

	public function addNewActivity(Request $request)
	{
		$activity = new AdminActivity;

		$validator = Validator::make($request->all(), [
			'title' => 'required|min:3',
			'description' => 'required|min:3',
			'datetime' => 'required|date',
			'spaces' => 'required|numeric',
			'duration' => 'required|numeric',
			'price' => 'required|numeric',
            'postal_code' => 'required'
		]);

		if($validator->fails())
		{
			return redirect('admin/activity-add')
				->withErrors($validator)
				->withInput();
		}


        //$activity->qEnvOID = $request->input('environment');
        $activity->qProfOIDCreated = $request->input('profile');
        $activity->qTitle = $request->input('title');
        $activity->qDescription = $request->input('description');
        $activity->qLocation = $request->input('latlon').' | '.$request->input('postal_code').', '.$request->input('city');

        $dateparse = explode(' ', $request->input('datetime'));

        $activity->qDate = date('Y-m-d', strtotime($dateparse[0]));
        $activity->qTime = $dateparse[1];

        if($request->input('active') == 1)
        {
        	$activity->qIsActive = 0;
        } else {
        	$activity->qIsActive = 1;
        }

        if($request->input('public') == 1)
        {
        	$activity->qPublicYN = 1;
        } else {
        	$activity->qPublicYN = 0;
        }

        if($request->input('outside') == 1)
        {
        	$activity->qOutsideYN = 1;
        } else {
        	$activity->qOutsideYN = 0;
        }

        $activity->qNofSpaces = $request->input('spaces');
        $activity->qURLLocation = $request->input('locationurl');
        $activity->qDuration = $request->input('duration');
        $activity->qPrice = $request->input('price');
        $activity->qCreatedAt = date('Y-m-d H:i:s');
        $activity->qModifiedAt = date('Y-m-d H:i:s');

        $activity->save();

        Session::flash('flash_message', 'Activity is successfully added');

        return redirect('admin/activities');

	}

	public function editActivity($id)
	{
		$users = DB::table('profiles')->where('qIsAdmin', '=', 0)->get();
		$activity = DB::table('activities')->where('qActivOID', '=', $id)->first();

		return view('admin.activity-edit', compact('users', 'activity'));
	}


	public function editThisActivity($id, Request $request)
	{
		$activity = AdminActivity::find($id);

		$validator = Validator::make($request->all(), [
			'title' => 'required|min:3',
			'description' => 'required|min:3',
			'datetime' => 'required|date',
			'spaces' => 'required|numeric',
			'duration' => 'required|numeric',
			'price' => 'required|numeric',
            'postal_code' => 'required'
		]);

		if($validator->fails())
		{
			return redirect('admin/activity-edit/' . $activity->qActivOID)
				->withErrors($validator)
				->withInput();
		}

		if($request->file('image') && substr($request->file('image')->getMimeType(), 0, 5) == 'image') {
            $file = $request->file('image');
            $filename = uniqid() . $file->getClientOriginalName();

            if(!file_exists('images/activities')) {
                mkdir('images/activities/', 0777, true);
            }
            $file->move('images/activities', $filename);

            if(!file_exists('images/activities/thumbs')) {
                mkdir('images/activities/thumbs', 0777, true);
            }
            $thumb = Image::make('images/activities/' . $filename);
            $thumb->resize(null, 200, function ($constraint) {
			    $constraint->aspectRatio();
			});
			$thumb->save('images/activities/thumbs/' . $filename, 50);

            $activity->qImage = $filename;
        }

        //$activity->qEnvOID = $request->input('environment');
        $activity->qProfOIDCreated = $request->input('profile');
        $activity->qTitle = $request->input('title');
        $activity->qDescription = $request->input('description');
        $activity->qLocation = $request->input('latlon').' | '.$request->input('postal_code').', '.$request->input('city');

        $dateparse = explode(' ', $request->input('datetime'));

        $activity->qDate = date('Y-m-d', strtotime($dateparse[0]));
        $activity->qTime = $dateparse[1];

        if($request->input('active') == 1)
        {
        	$activity->qIsActive = 0;
        } else {
        	$activity->qIsActive = 1;
        }

        if($request->input('public') == 1)
        {
        	$activity->qPublicYN = 1;
        } else {
        	$activity->qPublicYN = 0;
        }

        if($request->input('outside') == 1)
        {
        	$activity->qOutsideYN = 1;
        } else {
        	$activity->qOutsideYN = 0;
        }

        $activity->qNofSpaces = $request->input('spaces');
        $activity->qURLLocation = $request->input('locationurl');
        $activity->qDuration = $request->input('duration');
        $activity->qPrice = $request->input('price');
        $activity->qModifiedAt = date('Y-m-d H:i:s');

        $activity->save();

        Session::flash('flash_message', 'Activity is successfully edited');

        return redirect('admin/activities');

	}

	public function deleteActivity($id)
	{
		$activity = AdminActivity::find($id);

		if($activity)
		{
			$activity->delete();

            DB::table('activbookings')
                ->where('qActivOID', '=', $id)
                ->delete();

            DB::table('activcomments')
                ->where('qActivOID', '=', $id)
                ->delete();

            DB::table('activtags')
                ->where('qActivOID', '=', $id)
                ->delete();
		}

		Session::flash('flash_message', 'Activity has been successfully deleted');

		return redirect('admin/activities');
	}

}
