<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminActivity extends Model {

	protected $primaryKey = 'qActivOID';
	public $timestamps = false;

	protected $table = 'activities';

	public function user()
	{
		return $this->belongsTo('App\User', 'qProfOIDCreated', 'qProfOID');
	}


}
