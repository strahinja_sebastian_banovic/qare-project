@extends('app-front')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="page-title">Inbox</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 pull-right">
					<a class="btn btn-info" href="{{ url('chatinvites') }}">
						Chat invites
					</a>
				</div>
			</div>

			<br>
			@if(count($chat) > 0)
			<div class="row">
				<div class="col-md-12">
					@foreach($chat as $singlechat)

					<?php
						if($singlechat->qProfOID == Auth::user()->qProfOID)
						{
							$chatuser = DB::table('profiles')->where('qProfOID', '=', $singlechat->qProfOIDRequested)->first();
						} else {
							$chatuser = DB::table('profiles')->where('qProfOID', '=', $singlechat->qProfOID)->first();
						}

						$lastchatmessage = DB::table('chatmessages')->where('qChatOID', '=', $singlechat->qChatOID)->orderBy('qCreatedAt', 'DESC')->first();
					?>
					<a href="{{ url('messages/'.$chatuser->qProfOID) }}">
						<div class="row inbox
							@if($lastchatmessage)
								@if($lastchatmessage->qSeen == 0 && $lastchatmessage->qProfOIDReceiver == Auth::user()->qProfOID) {{ 'unseen' }} @endif
							@endif
						">
							<div class="col-md-2 col-xs-2 image">
								@if($chatuser->qPicture)
									<img src="{{ url('images/users/thumbs/'.$chatuser->qPicture) }}" >
								@else
									@if($chatuser->qGender==0 || $chatuser->qGender==1)
										<img src="{{ url('img/user-male.png') }}">
									@else
										<img src="{{ url('img/user-female.png') }}">
									@endif
								@endif
							</div>
							<div class="col-md-4 col-xs-9">


								@if($lastchatmessage)
	 								<p>{{ e(str_limit($lastchatmessage->qMessage, 30, '...')) }}</p>
								@else
									<p>Click here to start conversation.</p>
								@endif
							</div>
							<div class="col-md-2 col-xs-1">
								@if($lastchatmessage)
								 	@if($lastchatmessage->qProfOIDReceiver == Auth::user()->qProfOID) <i class="glyphicon glyphicon-download"></i> @else <i class="glyphicon glyphicon-upload"></i> @endif
								 @endif
							</div>
						</div>
					</a>
					@endforeach
				</div>
			</div>
			@else
			<div class="row">
				<div class="col-md-12 text-center">
					Your inbox is empty.
				</div>
			</div>
			@endif
		</div>
	</div>
</div>

@endsection
