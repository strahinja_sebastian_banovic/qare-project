@extends('app-front')

@section('content')

<div class="container addactcontainer">

    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
            <h1 class="page-title addnewact">Maak oproep</h1>
        </div>
    </div>

    @if(count($errors))
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ ($error) }}</p>
        @endforeach
    </div>
    @endif

    @if (session('flash_message'))
        <div class="alert alert-success">{{ session('flash_message') }}</div>
    @endif

    @if (session('error_message'))
        <div class="alert alert-danger">{{ session('error_message') }}</div>
    @endif

<form method="post" action="{{ url('add-activity') }}" class="addactform">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center col-sm-12">
            <label>&nbsp;</label>
            <input type="text" name="title" value="{{ old('title') }}" class="form-control" placeholder="Activity title">
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <label>&nbsp;</label>
                    <input type="text" id="autocomplete" name="location" value="{{ old('location') }}" onFocus="geolocate()" class="form-control" placeholder="Search for city or zip code">
                    <input type="hidden" name="latlon" value="{{ old('latlon') }}" id="longitude">
                    <input type="hidden" name="city" value="{{ old('city') }}" id="city">
                </div>
                <div class="col-md-4 col-xs-12">
                    <label>&nbsp;</label>
                    <input type="text" name="postal_code" class="form-control" value="" placeholder="Zip code" id="postal_code">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12 col-md-offset-3">
            <label>&nbsp;</label>
            <input type="text" name="date" value="{{ old('date') }}" class="form-control date" placeholder="Date">
        </div>
        <div class="col-md-2 col-xs-12">
            <label>&nbsp;</label>
            <input type="text" name="time" value="{{ old('time') }}" class="form-control timepicker" placeholder="Time">
        </div>

    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <label>&nbsp;</label>
            <textarea name="description" placeholder="Description" class="form-control" rows="5">{{ old('description') }}</textarea>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <label>Interesse tags</label>
            <div class="row input_fields_wrap">
                <div class="col-md-12">
                    <a class="add_field_button btn btn-primary btn-xs addnewtagbtn">Add a new tag</a>
                    <label>&nbsp;</label>
                </div>
            <?php
                $usertags = DB::table('proftags')
                    ->where('qProfOID', '=', Auth::user()->qProfOID)
                    ->get();

                if(count($usertags) > 0){
            ?>
                @foreach($usertags as $tags)

                <?php
                    $tag = DB::table('envtags')
                        ->where('qTagOID', '=', $tags->qTagOID)
                        ->first();
                ?>
                <div class="col-md-6 col-xs-12 tags">
                    <div class="input-group">
                        <input type="text" name="tags[]" value="{{ $tag->qTagDesc }}" class="form-control" placeholder="Tag">
                        <span class="input-group-btn remove_field">
                            <a  href="#" class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </a>
                        </span>
                    </div>
                </div>
                @endforeach

            <?php } ?>

            </div>
        </div>
    </div>

    <hr>

    <div class="row customradio labelstyle">
        <div class="col-md-6 col-md-offset-3">
            <div class="col-md-6 col-xs-6">
                <input type="radio" id="publicsearch" name="publicsearch" value="0" @if(old('publicsearch') == 0) {{ 'checked="ckecked"' }} @endif >
                <label for="publicsearch"><span></span> Home</label>
            </div>
            <div class="col-md-6 col-xs-6">
                <input type="radio" id="outside" name="outside" value="0" @if(old('outsidesearch') == 0) {{ 'checked="ckecked"' }} @endif >
                <label for="outside"><span></span> Inside</label>
            </div>
        </div>
    </div>

    <div class="row customradio labelstyle">
        <div class="col-md-6 col-md-offset-3">
            <div class="col-md-6 col-xs-6">
                <input type="radio" id="publicsearch2" name="publicsearch" value="1" @if(old('publicsearch') == 1 || !old('publicsearch')) {{ 'checked="ckecked"' }} @endif >
                <label for="publicsearch2"><span></span> Public</label>
            </div>
            <div class="col-md-6 col-xs-6">
                <input type="radio" id="outside2"  name="outside" value="1" @if(old('outsidesearch') == 1 || !old('outsidesearch')) {{ 'checked="ckecked"' }} @endif >
                <label for="outside2"><span></span> Outside</label>
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <label>Available spaces</label>
                    <input type="number" name="spaces" value="{{ old('spaces') }}" class="form-control" min="0">
                </div>

                <div class="col-md-3 col-xs-12">
                    <label>Duration in hours</label>
                    <input type="number" name="duration" value="{{ old('duration') }}" class="form-control" min="0">
                </div>

                <div class="col-md-3 col-xs-12">
                    <label>Price</label>
                    <input type="number" name="price" value="{{ old('price') }}" class="form-control" min="0">
                </div>
            </div>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <input type="text" name="locationurl" value="{{ old('locationurl') }}" class="form-control" placeholder="Location url">
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center dashboard">
            <input type="submit" class="btn btn-primary btn-block" value="Save">
        </div>
    </div>

</form>
    <div style="padding-bottom:100px;"></div>
</div>

@endsection

@section('tailscripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&libraries=geometry,places&callback=initAutocomplete&language=en" async defer></script>
<script>

    var placeSearch, autocomplete;

    function initAutocomplete() {

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types: ['geocode']});

        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        $("#longitude").val(place.geometry.location.lat() + ', ' + place.geometry.location.lng());


        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            if(addressType == 'locality')
            {
                $("#city").val(place.address_components[i].long_name);
            }

            if(addressType == 'postal_code')
            {
                $("#postal_code").val(place.address_components[i].long_name);
            } else {
                $("#postal_code").val('');
            }

        }
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>


<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 10;
        var wrapper         = $(".input_fields_wrap");
        var add_button      = $(".add_field_button");

        var x = 0;
        $(add_button).click(function(e){
            e.preventDefault();
            if(x < max_fields){
                x++;
                $(wrapper).append('<div class="col-md-6 col-xs-12 tags"><div class="input-group"><input type="text" name="tags[]"  class="form-control" placeholder="Tag"><span class="input-group-btn remove_field"><a  href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></span></div></div>');
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent().parent('div').remove(); x--;
        })
    });
</script>
@endsection
