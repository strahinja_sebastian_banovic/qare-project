<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Qare project</title>
    <style type="text/css">
        body{width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;background:#ffffff}
        * {-webkit-font-smoothing:antialiased}
        .ExternalClass * {line-height:100%}
        table td {border-collapse:collapse}
        *[class].wlf {width:600px}
        *[class].w-280 {width:280px}
        *[class].w-200 {width:200px}
        *[class].w-150 {width:150px}
        *[class].w-130 {width:130px}
        *[class].w-120 {width:120px}
        *[class].w-30 {width:30px}
        @media only screen and (max-width:600px) {
            *[class].full-width {width:100% !important}
            *[class].m-width {width:100% !important;max-width:100% !important}
        }

    </style>
</head>
<body>
    <table width="100%" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed">
        <tr>
            <td>
                <table class="wlf full-width" width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;max-width:600px">
                    <tr>
                        <td>


                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#337ab7; padding: 20px; font-family: sans-serif;">
                            <tr >
                                <td  style="padding: 6px ; font-size:24px; color:#fff"><?php echo $parent->questionTitle; ?></td>

                            </tr>
                        </table>


                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #EBEBEB;padding: 20px;">
                        <tr>
                            <td width="14%">&nbsp;</td>
                            <td>
                                <table width="100%"  border="0" cellspacing="0" cellpadding="0" style="font-family: sans-serif;">

                                    <tr>
                                        <td><?php echo $parent->questionText; ?></td>
                                    </tr>

                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <th>Answer</th>
                                    </tr>

                                    <tr>

                                        <td><?php echo $child->questionText; ?></td>
                                    </tr>


                                    <tr>
                                        <td height="25">&nbsp;</td>
                                    </tr>

                                    <tr>
                                            <td align="center" style="color:#666666;font-family:Arial, sans-serif;font-size:11px;line-height:16px">Copyright &copy;  2015 Qare project.</td>
                                        </tr>

                                </table>
                            </td>
                            <td width="14%">&nbsp;</td>
                        </tr>
                    </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
