@extends('app-front')

@section('content')

<div class="container">

	<?php 
	$userID = $user->qProfOID;
	?>
	
	<div class="row">
		<div class="page-title text-centerd dashtitle">
			<h1>Dashboard</h1>
		</div>
	</div>

	<div class="row dashboard-container">
		
		<?php 
		$messageCount = DB::table('chatmessages')
		->where('qProfOIDReceiver', $userID) 
		->where('qSeen', 0) 
		->count();

		$commentCount = DB::table('profcomments')
		->where('qProfOID', $userID) 
		->count();

		$chatCount = DB::table('chat')
		->where('qProfOID', $userID) 
		->where('qStatus', 0) 
		->count();

		$pending =  DB::table('activbookings')
		->where('qProfOIDBookingCreated', $userID) 
		->where('qStatus', 0) 
		->count();

		$accepted =  DB::table('activbookings')
		->where('qProfOIDBookingCreated', $userID) 
		->where('qStatus', 1) 
		->count();

		$declined =  DB::table('activbookings')
		->where('qProfOIDBookingCreated', $userID) 
		->where('qStatus', 2) 
		->count();

		$comments =  DB::table('activcomments')
		->where('qProfOIDActivCreated', $userID) 
		->count();
		?>

		<div class="col-sm-3 col-xs-12 leftdash">
      <div class="leftdashborder"></div>
    
      <div class="leftdashinner">
        <div class="row imgtext">

            <div class="col-sm-12 col-xs-6">
      			@if($user->qPicture)
      				<img src="{{ url('images/users/thumbs/'.$user->qPicture) }}" class="img-rounded" width="200px">
      			@else
      				@if($user->qGender==0 || $user->qGender==1)
      					<img src="{{ url('img/user-male.png') }}" class="img-rounded" width="200px">
      				@else
      					<img src="{{ url('img/user-female.png') }}" class="img-rounded" width="200px">
      				@endif
      			@endif
            </div>

            <div class="col-sm-12 col-xs-6">
                  <p class="welkom">Welkom terug {{ $user->qNameFirst }}!</p>
                  @if($messageCount > 0) 
      				<p>Je hebt <span class="orange-underline"><a href="{{url('/messages')}}">{{ $messageCount }} </a></span> nieuwe berichten</p>
      			@else
      				<p>Je hebt <span class="orange-underline">0</span> nieuwe berichten</p>
      			@endif
            </div>

            <div class="clear"></div>

            <div class="smallborder"></div>

        </div> <!-- /.row -->


            <p class="bold overzicht">OVERZICHT ACTIVITIEN</p>
            <div class="overact">
                <div>
                  <a href="{{url('/invites')}}" class="btn btn-default btn-yellow oa"><span class="glyphicon glyphicon-ok-circle"></span> Pending 
                    @if ($pending > 0)
                    <span class="badge badge-notify">{{ $pending }}</span>
                    @endif
                  </a>
                </div>
                <div>
                  <a href="{{url('/activities')}}" class="btn btn-default btn-green oa"><span class="glyphicon glyphicon-ok"></span> Afgerond 
                    @if ($accepted > 0)
                    <span class="badge badge-notify">{{ $accepted }}</span>
                    @endif
                  </a>
                </div>
                <div>
                  <a href="{{url('/messages')}}" class="btn btn-info btn-blue oa"><span class="glyphicon glyphicon-comment"></span> Reviews 
                    @if ($comments > 0)
                    <span class="badge badge-notify">{{ $comments }}</span>
                    @endif
                  </a>
                </div>
                <div>
                  <a href="{{url('/activities')}}" class="btn btn-default btn-red oa"><span class="glyphicon glyphicon-remove"></span> Canceled 
                    @if ($declined > 0)
                    <span class="badge badge-notify">{{ $declined }}</span>
                    @endif
                  </a>
                </div>
            </div> <!-- /.overact -->

			
			@if ($percentage < 100)

            <div class="smallborder two"></div>

            <p class="bold vul">VUL JE PROFIEL AAN</p>
			
            <p class="progres-percent"><span class="zero">{{ $percentage }}%</span><span style="float: right; class=" class="hundred">100%</span></p>
            <div class="progress">
              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentage }}%">
                <span class="sr-only">{{ $percentage }}% </span>
              </div>
            </div>

            <a href="{{url('/profile')}}" class="btn btn-default btn-dark profperc"><span class="glyphicon glyphicon-plus"></span> VUL PROFIEL AAN</a>
			@endif
            <div class="dpb" style="padding-bottom: 100px;"></div>
            
            </div> <!-- /.leftdashinner -->

         </div> <!-- /.col-md3 -->

         <div class="mobreviewtitle">
            <h2 class="dashboard-reviews-title">Reviews</h2>
         </div>

         <div class="col-sm-9 col-xs-12 bottom-align rightdash">

            <h2 class="dashboard-reviews-title">Reviews</h2>
            
            <?php 
              $comments = DB::table('profcomments')->where('qProfOIDCreated', $userID)->get();
              ?>

            <p><span class="grey">Openstaande</span> reviews <span class="badge">{{ count($comments) }}</span></p>            

            <table class="table table-condensed no-border-dashboard">
              <tbody><tr>
                @foreach ($comments as $comment)
                <tr>
                  <th>{{ $comment->qCreatedAt }}</th>
                  <td>{{ $comment->qComment }}</td>
                </tr>
                @endforeach
            </tbody></table>
            <p><span class="grey">Door jou</span> geschreven reviews</p>

            <table class="table table-condensed no-border-dashboard">
              
              <tbody>
                @foreach ($comments as $comment)
                <tr>
                  <th>{{ $comment->qCreatedAt }}</th>
                  <td>{{ $comment->qComment }}</td>
                </tr>
                @endforeach

            </tbody></table>

            <p><span class="grey">Over jou</span> geschreven reviews</p>

            <table class="table table-condensed no-border-dashboard">
              
              <tbody>
                @foreach ($comments as $comment)
                <tr>
                  <th>{{ $comment->qCreatedAt }}</th>
                  <td>{{ $comment->qComment }}</td>
                </tr>
                @endforeach

            </tbody></table>
            <br>

          </div> <!-- /.col-md bottom-align -->
  <div class="clear"></div>
        
	</div> <!-- /.row dashboard-container -->


</div> {{-- end .container --}}
@endsection
