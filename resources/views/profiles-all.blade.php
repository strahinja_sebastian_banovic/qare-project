@extends('app-front')

@section('content')

<div class="container">
    <div class="row">
        
		<div class="page-title text-center">
            <h1 class="profilesh1">Profiles</h1>
        </div>

        <div class="search-form">
			
			<a class="actsearch"  href="{{ url('profiles-search') }}">
				<i class="btn glyphicon glyphicon-search"></i> ZOEK
			</a>
			<div class="clear"></div>
			@if(count($profiles) > 0)
			<div class="row favorite-row">
			
			@foreach($profiles as $profile)

			<?php
				$numofactivities = DB::table('activities')->where('qProfOIDCreated', '=', $profile->qProfOID)->whereRaw("CONCAT('qDate', ' ', 'qTime') >= '".date('Y-m-d H:i:s')."'")->count();
			?>


			<div class="col-md-6 col-sm-6 col-xs-12">

				    <div class="favorites-image">
				    	<a href="{{ url('view-profile/'.$profile->qProfOID) }}">
                  		@if($profile->qPicture)
							<img class="img-rounded" src="{{ url('images/users/thumbs/'.$profile->qPicture) }}">
						@else
							@if($profile->qGender==0 || $profile->qGender==1)
								<img class="img-rounded" src="{{ url('img/user-male.png') }}">
							@else
								<img class="img-rounded" src="{{ url('img/user-female.png') }}">
							@endif
						@endif
						</a>
	                    <div class="favorites-info">
	                    	<?php 
							$year = substr($profile->qDateOfBirth ,0 ,4);
							if($year == '0000')
							{
								$year = 'Not set.';
							} else {
								$datetime1 = date_create(date('Y-m-d'));
								$datetime2 = date_create(date('Y-m-d', strtotime($profile->qDateOfBirth)));
								$interval = date_diff($datetime1, $datetime2);
								$year = $interval->y;
							}
							?>
		                      <div class="favorites-name">{{ $profile->qNameFirst }}, {{ $year }}</div>
		                      <div class="favorites-place">{{ $profile->qCity }}</div>
		                      <?php 
			                    $commentCount = DB::table('activcomments')->where('qProfOIDActivCreated', $profile->qProfOID)->count();
			                    ?>
		                      <div class="favorites-reviews">{{ $commentCount }} reviews</div>
		                      {{-- <div class="favorites">{{ $numofactivities }} activities</div> --}}
	                    </div>
                  	</div>
				</div> <!-- /.col-md-6 col-sm-6 col-xs-12 -->


			@endforeach

			{!! str_replace('/?', '?', $profiles->appends(Input::only('tags', 'male', 'female', 'location', 'distance'))->render()) !!}
			</div>
			
			@else
			<h3 class="page-body-title">No profiles at this moment...</h3>
			@endif

        </div> <!-- /.search-form -->

    </div> <!-- /.row -->
</div> <!-- /.container -->

@endsection
