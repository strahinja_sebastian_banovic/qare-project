@extends('app-front')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-center">
			<h1 class="page-title">Chat invites</h1>
		</div>
	</div>

	@if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
     @endif

	@if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
     @endif

     <div class="row">
     	<div class="col-md-12 pull-right">
     		<a class="btn btn-info" href="{{ url('invites') }}">
     			Invites
     		</a>
     	</div>
     </div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			@if(count($invites) > 0)

			@foreach($invites as $invite)

			<?php
				if($invite->qProfOIDRequested == Auth::user()->qProfOID)
				{
					$userinvite = App\User::find($invite->qProfOID);
				}
				else {
					$userinvite = App\User::find($invite->qProfOIDRequested);
				}
			?>

				<div class="row activity">
					<div class="col-md-4 col-xs-12">
						<h3 class="page-body-title">{{ $userinvite->qNameFirst.' '.$userinvite->qNameLast }}</h3>
						<div class="image text-center">

							@if($invite->qStatus == 0)
								<p class="label label-info">Pending</p>
							@elseif($invite->qStatus == 1)
								<p class="label label-success">Accepted</p>
							@elseif($invite->qStatus == 2)
								<p class="label label-danger">Declined</p>
							@endif

							<a href="{{ url('view-profile/'.$userinvite->qProfOID) }}">
								@if($userinvite->qPicture)
									<img src="{{ url('images/users/thumbs/'.$userinvite->qPicture) }}">
								@else
									@if($userinvite->qGender==0 || $userinvite->qGender==1)
										<img src="{{ url('img/user-male.png') }}">
									@else
										<img src="{{ url('img/user-female.png') }}">
									@endif
								@endif
							</a>
						</div>
					</div>
					@if($invite->qProfOID == Auth::user()->qProfOID && $invite->qStatus == 0)
					<div class="col-md-8 col-xs-12">
						<div class="row prev-meta">
							<div class="col-md-6 col-xs-12 text-center">
								<a class="btn btn-success" href="{{ url('chatinvites/'.$invite->qChatOID.'/accept') }}" onclick="return confirm('Are you sure you want to accept this request?')"><i class="glyphicon glyphicon-ok"></i> Accept</a>
								<a class="btn btn-danger" href="{{ url('chatinvites/'.$invite->qChatOID.'/decline') }}" onclick="return confirm('Are you sure you want to decline this request?')"><i class="glyphicon glyphicon-remove"></i> Decline</a>
							</div>

						</div>
					</div>
					@endif
				</div>
			@endforeach

			{!! str_replace('/?', '?', $invites->render()) !!}

			@else
			<h3 class="page-body-title">No invites for chat at this moment...</h3>
			@endif

		</div>
	</div>
</div>

@endsection
