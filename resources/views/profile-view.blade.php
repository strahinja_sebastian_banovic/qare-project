@extends('app-front')

@section('content')

<div class="container-fluid">

	@if (session('flash_message'))
	<div class="alert alert-success">{{ session('flash_message') }}</div>
	@endif

	@if (session('error_message'))
	<div class="alert alert-danger">{{ session('error_message') }}</div>
	@endif

	@if(count($errors))
		<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			{{ ($error) }}
		@endforeach
		</div>
	@endif

    <div class="row userpfofile">
        
		<div class="page-title text-center profiel">
			@if($user->qProfOID == Auth::user()->qProfOID)
			<h1>Mijn Profiel</h1>
			@else
			<h1>Profiel</h1>
			@endif
			
			@if($user->qProfOID != Auth::user()->qProfOID)
            <div class="maakmsgprofiel notown">
            @else
			<div class="maakmsgprofiel own">
            @endif
            	
            </div>

         </div> <!-- /.page-title text-center -->


              @if($user->qPicture)
              <div class="image-section" style="background:url({{ url('images/users/thumbs/'.$user->qPicture) }});background-size:cover;">
				@else
					@if($user->qGender==0 || $user->qGender==1)
						<div class="image-section" style="background:url({{ url('img/user-male.png') }});background-size:cover;">
					@else
						<img class="img-rounded" src="{{ url('img/user-female.png') }}">
					@endif
				@endif
              <div class="image-overlay profiel">
                <div class="overlay-left">
                  @if($user->qProfileMemberLevel == 0)
					<button class="btn btn-info btn-regular">REGULAR MEMBER</button>
					@else
					<button class="btn btn-info btn-premium">PREMIUM MEMBER</button>
					@endif
					<?php 
					$year = substr($user->qDateOfBirth ,0 ,4);
					if($year == '0000')
					{
						$year = 'Not set.';
					} else {
						$datetime1 = date_create(date('Y-m-d'));
						$datetime2 = date_create(date('Y-m-d', strtotime($user->qDateOfBirth)));
						$interval = date_diff($datetime1, $datetime2);
						$year = $interval->y;
					}
					?>
                  <h3 class="profile-name">{{ $user->qNameFirst }}, {{ $year }}</h3>
                </div>
                @if($user->qProfOID != Auth::user()->qProfOID)
	            <div class="overlay-right profiel notown">
	            @else
				<div class="overlay-right profiel own">
	            @endif
                  <div class="button-right greenbtn">
                    @if($user->qProfOID != Auth::user()->qProfOID)

					<?php
						$requestcheck = DB::table('chat')
							->where('qProfOIDRequested', '=', Auth::user()->qProfOID)
							->where('qProfOID', '=', $user->qProfOID)
							->where('qStatus', '<>', 2)
							->first();

						$secondrequestcheck = DB::table('chat')
							->where('qProfOID', '=', Auth::user()->qProfOID)
							->where('qProfOIDRequested', '=', $user->qProfOID)
							->where('qStatus', '<>', 2)
							->first();
					?>

						@if($requestcheck)

							@if($requestcheck->qStatus == 0)
								<button class="btn btn-default btn-green-envelope"><i class="glyphicon glyphicon-envelope"></i> Requested</button>
							@elseif($requestcheck->qStatus == 1)
								<a href="{{ url('messages/'.$user->qProfOID) }}" class="btn btn-default msgbtn btn-green-envelope"><i class="glyphicon glyphicon-envelope"></i> STUUR BERICHT</a>
							@else

							@endif

						@elseif($secondrequestcheck)

							@if($secondrequestcheck->qStatus == 0)
								<button class="btn btn-default btn-green-envelope" data-toggle="modal" data-target="#chatrequestedit"><i class="glyphicon glyphicon-envelope"></i> Confirm</button>

								<div class="modal fade" id="chatrequestedit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
									<div class="modal-dialog">
										<div class="modal-content text-center">
											<div class="modal-header">
												<h3>Chat request from {{ $user->qNameFirst.' '.$user->qNameLast }}</h3>
											</div>
											<div class="modal-body">
												<a href="{{ url('chatrequest/'.$user->qProfOID.'/accept') }}" onclick="return confirm('Are you sure you want to accept?')" class="btn btn-success">Accept chat request</a>
												<a href="{{ url('chatrequest/'.$user->qProfOID.'/decline') }}" onclick="return confirm('Are you sure you want to decline?')" class="btn btn-danger">Decline chat request</a>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							@elseif($secondrequestcheck->qStatus == 1)
								<a href="{{ url('messages/'.$user->qProfOID) }}" class="btn btn-default btn-green-envelope"><i class="glyphicon glyphicon-envelope"></i> STUUR BERICHT</a>
							@else
								<button class="btn btn-default btn-green-envelope" data-toggle="modal" data-target="#chatrequest"><i class="glyphicon glyphicon-envelope"></i> Request</button>
								<div class="modal fade" id="chatrequest" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
									<div class="modal-dialog">
										<div class="modal-content text-center">
											<div class="modal-header">
												<h3>Send chat request to {{ $user->qNameFirst.' '.$user->qNameLast }}</h3>
											</div>
											<div class="modal-body">
												<form method="post" action="{{ url('view-profile/'.$user->qProfOID).'/chat' }}">
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<button type="submit" name="send_chat_request" class="btn btn-primary">Send chat request</button>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>


							@endif

						@else
							<button class="btn btn-default btn-green-envelope" data-toggle="modal" data-target="#chatrequest"><i class="glyphicon glyphicon-envelope"></i> Request</button>
							<div class="modal fade" id="chatrequest" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
								<div class="modal-dialog">
									<div class="modal-content text-center">
										<div class="modal-header">
											<h3>Send chat request to {{ $user->qNameFirst.' '.$user->qNameLast }}</h3>
										</div>
										<div class="modal-body">
											<form method="post" action="{{ url('view-profile/'.$user->qProfOID).'/chat' }}">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<button type="submit" name="send_chat_request" class="btn btn-primary">Send chat request</button>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>

					@endif
				@else
					<div class="row">
						<div class="col-md-12">
							<a href="{{ url('profile') }}" class="btn btn-default ep"><i class="glyphicon glyphicon-pencil"></i> Edit profiel</a>
						</div>
					</div>
				@endif
                  </div>
                  <div class="button-right orangebtn">
                    @if($user->qProfOID != Auth::user()->qProfOID)

				<?php
					$check = DB::table('proffavorites')
				        ->where('qProfOID', '=', Auth::user()->qProfOID)
				        ->where('qProfFavoritesNID', '=', $user->qProfOID)
			            ->where('qOIDFavorite', '=', $user->qProfOID)
				        ->where('qTypeOfFavorite', '=', 0)
				        ->first();

				    if(!$check){
				?>
				<form method="post" action="{{ url('view-profile/'.$user->qProfOID.'/favorite') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button class="favorite-btn btn btn-default btn-orange-star" type="submit" onclick="return confirm('Are you sure you want to add this profile to favorites?')"><i class="glyphicon glyphicon-star-empty"></i> MAAK FAVORIET</button>
				</form>
				<?php } else { ?>
				<button class="btn btn-default btn-orange-star"><i class="glyphicon glyphicon-star"></i> FAVORIET</button>
				<?php } ?>
			</div>
			@endif
                  </div>
                </div>
         </div> <!-- /.image-section -->

    </div> <!-- /.row -->

    <div class="row profile-row">
          <div class="col-md-6 col-sm-6 col-xs-12 left">
            <h4 class="orange-title">Over mij:</h4>
              <table class="table no-border wool">
                <tbody><tr>
                  <th>WOONPLAATS</th>
                  <th>LID SINDS</th>
                </tr>
                <tr>
                  <td>{{ $user->qCity }}</td>
                  <?php 
                  $date = strtotime($user->qCreatedAt);
                  $date = date('F Y', $date);
                  ?>
                  <td>{{ $date }}</td>
                </tr>
                <tr>
                	<?php
							$numberofbookings = DB::table('activbookings')
								->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
								->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
								->where('profiles.qProfOID', '=', $user->qProfOID)
								->count();
						?>
                  <th>ASFPRAKEN GEHAD</th>
                </tr>
                <tr>
                  <td>{{ $numberofbookings }}</td>
                </tr>
              </tbody></table>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 right">

            <h4 class="orange-title">Interesse in:</h4>
            <?php
				$proftags = DB::table('proftags')->where('qProfOID', '=', $user->qProfOID)->get();
			?>
			@if(count($proftags) > 0)
						@foreach($proftags as $proftag)
							<?php
								$tag = DB::table('envtags')->where('qTagOID', '=', $proftag->qTagOID)->first();
							?>
							<button class="btn btn-info btn-tag">{{ $tag->qTagDesc }}</button>
						@endforeach
			@endif
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 bottom">
            <p class="profile-info">{{ $user->qDescription }}</p>
          </div>
    </div> <!-- /.row -->

    <div class="row">
        @if($user->qProfOID != Auth::user()->qProfOID)
			<div class="row">
				<div class="col-md-12">
					<?php
						$no = true;
						$check = DB::table('activbookings')
							->where('activbookings.qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
							->join('activities', function($join) use ($user)
						        {
						            $join->on('activities.qActivOID', '=', 'activbookings.qActivOID')
						                 ->on(DB::raw("CONCAT(activities.qDate, ' ', activities.qTime)<='".date('Y-m-d H:i:s')."'"), DB::raw(''), DB::raw(''));
						        })
							->orderBy('activbookings.qCreatedAt', 'DESC')
							->select('activbookings.*')
							->first();


						if($check)
						{

							$activity = DB::table('activities')
								->where('qActivOID', '=', $check->qActivOID)
								->first();

							$activitycreator = DB::table('profiles')
								->where('qProfOID', '=', $activity->qProfOIDCreated)
								->first();

							$checkifhavecomment = DB::table('profcomments')
								->where('qProfOID', '=', $activitycreator->qProfOID)
								->where('qProfCommentsNID', '=', $activity->qActivOID)
								->where('qProfOIDCreated', '=', Auth::user()->qProfOID)
								->first();
						}
					?>


					@if(count($comments) > 0)
						<div class="row">
							<div class="col-md-12">
								<h5>Profile comments</h5>
							</div>
						</div>

					@foreach($comments as $comment)
						<div class="row comment-div">

						<?php
							if($comment->tablename == 1)
							{
								$commented = DB::table('activities')
									->where('qActivOID', '=', $comment->whatiscommented)
									->first();

								$commentby = DB::table('profiles')
									->where('qProfOID', '=', $comment->commentedby)
									->first();

							} else if($comment->tablename == 2)
							{
								$commented = DB::table('activities')
									->where('qActivOID', '=', $comment->whatiscommented)
									->first();

								$commentby = DB::table('profiles')
									->where('qProfOID', '=', $comment->commentedby)
									->first();
							}
						?>

							<div class="col-md-6 col-xs-12">
								<div class="comment-meta">
									<a href="{{ url('view-profile/'.$commentby->qProfOID) }}">
										<span class="name"><i class="glyphicon glyphicon-comment"></i> {{ $commentby->qNameFirst.' '.$commentby->qNameLast }}</span>
									</a>
									<span class="comment-date"><i class="glyphicon glyphicon-calendar"></i> {{ date('d.m.Y. H:i', strtotime($comment->qCreatedAt)) }}</span>
								</div>
								<div class="comment-description">
									<div class="comment-image">

										<a href="{{ url('view-profile/'.$commentby->qProfOID) }}">
											@if($commentby->qPicture)
												<img src="{{ url('images/users/thumbs/'.$commentby->qPicture) }}" >
											@else
												@if($commentby->qGender==0 || $commentby->qGender==1)
													<img src="{{ url('img/user-male.png') }}">
												@else
													<img src="{{ url('img/user-female.png') }}">
												@endif
											@endif
										</a>

									</div>
									<div class="comment-text">
										@if($comment->tablename == 1)
											<b> {{ $commented->qTitle }}</b>
										@endif
										<p>{{ e($comment->qComment) }}</p>
									</div>
								</div>
							</div>

						</div>
					@endforeach

					@endif

					<?php
						$ckeckifcommented = DB::table('profcomments')
							->where('qProfOID', '=', $user->qProfOID)
							->where('qProfCommentsNID', '=', Auth::user()->qProfOID)
							->where('qProfOIDCreated', '=', Auth::user()->qProfOID)
							->first();
					?>
			
					@if(!$ckeckifcommented)
					
					@if($check)
					@if(! $no)
					<br>
				
					<h4>Leave a comment</h4>
					<form method="post" action="{{ url('profile-view/'.$activitycreator->qProfOID.'/comment') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-md-12">
								<textarea name="message" class="form-control" placeholder="Write something..."></textarea>
							</div>
						</div>

						<br>

						<div class="row">
							<div class="col-md-12 dashboard">
								<button class="btn btn-primary btn-block"><i class="glyphicon glyphicon-edit"></i> Write a comment</button>
							</div>
						</div>
					</form>


					@endif {{-- voja --}}
					@endif {{-- voja --}}
					@endif

				</div>
			</div>
			@endif
    </div> <!-- /.row -->

</div> <!-- /.container voja-->

@endsection
