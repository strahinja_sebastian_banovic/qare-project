@extends('app-front')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-center">
		<h1 class="page-title">Invite</h1>
		</div>
	</div>

	@if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
     @endif

	@if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
     @endif

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-md-6 col-xs-5 image-invite text-center">
					@if($bookinguser->qPicture)
						<img src="{{ url('images/users/thumbs/'.$bookinguser->qPicture) }}">
					@else
						@if($bookinguser->qGender==0 || $bookinguser->qGender==1)
							<img src="{{ url('img/user-male.png') }}">
						@else
							<img src="{{ url('img/user-female.png') }}">
						@endif
					@endif
					<h5>
						{{ $bookinguser->qNameFirst.' '.$bookinguser->qNameLast }}
					</h5>
				</div>
				<div class="col-md-2 col-xs-2 text-center">
					<h5>&raquo;</h5>
				</div>
				<div class="col-md-6 col-xs-5 image-invite text-center">
					@if($activityowner->qPicture)
						<img src="{{ url('images/users/thumbs/'.$activityowner->qPicture) }}">
					@else
						@if($activityowner->qGender==0 || $activityowner->qGender==1)
							<img src="{{ url('img/user-male.png') }}">
						@else
							<img src="{{ url('img/user-female.png') }}">
						@endif
					@endif
					<h5>
						{{ $activityowner->qNameFirst.' '.$activityowner->qNameLast }}
					</h5>
				</div>
			</div>


			<br>

			<div class="row prev-meta text-center">
				<div class="col-md-12">
					<p><b> {{ $bookinguser->qNameFirst.' '.$bookinguser->qNameLast }} </b> has booked the activity <b>"{{ $activity->qTitle }}"</b> with <b>{{ $activityowner->qNameFirst.' '.$activityowner->qNameLast }}</b> </p>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 text-center">
				@if($activityowner->qProfOID == Auth::user()->qProfOID)
					@if($booking->qStatus == 0)
						<a class="btn btn-success" href="{{ url('invites/'.$activity->qActivOID.'/'.$bookinguser->qProfOID.'/accept') }}" onclick="return confirm('Are you sure you want to accept?')"><i class="glyphicon glyphicon-ok"></i> Accept</a>
						<a class="btn btn-danger" href="{{ url('invites/'.$activity->qActivOID.'/'.$bookinguser->qProfOID.'/decline') }}" onclick="return confirm('Are you sure you want to refuse?')"><i class="glyphicon glyphicon-remove"></i> Refuse</a>
					@elseif($booking->qStatus==1)
						<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> Accepted</span>
					@else
						<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i> Declined</span>
					@endif

				@else

					@if($booking->qStatus==0)
						<span class="label label-info"><i class="glyphicon glyphicon-time"></i> Requested</span>
					@elseif($booking->qStatus==1)
						<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> Accepted</span>
					@else
						<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i> Declined</span>
					@endif

					@if($booking->qStatus == 0 && $booking->qProfOIDBookingCreated == Auth::user()->qProfOID)
						<a href="{{ url('invite/cancel/'.$booking->qActivOID) }}" class="btn btn-primary" onclick="return confirm('Are you sure you want to cancel this request?')">Cancel request</a>
					@endif

				@endif
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-md-12">
					<?php
						if($activity->qLocation != '')
						{
							$location = explode(' | ', $activity->qLocation);
							$locationshow = $location[1];
						} else {
							$locationshow = '';
						}
					?>

					<table class="table table-striped">
						<tr>
							<th>Location</th>
							<td>{{ $locationshow }}</td>
						</tr>
						<tr>
							<th>Date</th>
							<td>{{ date('d.m.Y.', strtotime($activity->qDate)) }}</td>
						</tr>
						<tr>
							<th>Time</th>
							<td>{{ date('H:i', strtotime($activity->qTime)) }}</td>
						</tr>
						<tr>
							<th>Category</th>
							<td>@if($activity->qOutsideYN == 1) {{ 'Outside' }} @else {{ 'Inside' }}  @endif</td>
						</tr>
						<?php
							$numofbooking = DB::table('activbookings')->where('qActivOID', '=', $activity->qActivOID)->where('qStatus', '=', 1)->count();
						?>
						<tr>
							<th>Free spaces</th>
							<td>{{ $activity->qNofSpaces-$numofbooking.'/'.$activity->qNofSpaces }}</td>
						</tr>
					</table>


				</div>
			</div>




			<br>
			@if($activityowner->qProfOID != Auth::user()->qProfOID)

			<?php
				$check = DB::table('activbookings')
					->where('activbookings.qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
					->where('activbookings.qActivOID', '=', $activity->qActivOID)
					->where('activbookings.qStatus', '=', 1)
					->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
					->whereRaw("CONCAT('activities.qDate', ' ', 'activities.qTime')>='".date('Y-m-d H:i:s')."'")
					->first();

				if($check){

					$checkifcommented = DB::table('activcomments')
						->where('qActivOID', '=', $activity->qActivOID)
						->where('qActivCommentsNID', '=', Auth::user()->qProfOID)
						->where('qProfOIDActivCreated', '=', $activity->qProfOIDCreated)
						->first();

						if(!$checkifcommented){
			?>
			<div class="row">
				<div class="col-md-12 text-center">
					<form method="post" action="{{ url('invite-comment/'.$activity->qActivOID) }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="activity" value="{{ $activity->qActivOID }}">
						<div class="row">
							<div class="col-md-12">
								<textarea name="message" class="form-control" placeholder="Write something..."></textarea>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12 dashboard">
								<button class="btn btn-primary"><i class="glyphicon glyphicon-edit"></i> Write a comment for this activity</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<?php } else { ?>

			<div class="row coment-div">
				<div class="col-md-6 col-xs-12">
					<div class="comment-meta">
						<span class="name"><i class="glyphicon glyphicon-comment"></i> {{ Auth::user()->qNameFirst.' '.Auth::user()->qNameLast }}</span>
						<span class="comment-date"><i class="glyphicon glyphicon-calendar"></i> {{ date('d.m.Y. H:i', strtotime($checkifcommented->qCreatedAt)) }}</span>
					</div>
					<div class="comment-description">
						<div class="comment-image">
							@if(Auth::user()->qPicture)
								<img src="{{ url('images/users/thumbs/'.Auth::user()->qPicture) }}" >
							@endif
						</div>
						<div class="comment-text">
							{{ e($checkifcommented->qComment) }}
						</div>
					</div>
				</div>

			</div>

			<?php } } ?>

			@endif

		</div>
	</div>

</div>

@endsection
