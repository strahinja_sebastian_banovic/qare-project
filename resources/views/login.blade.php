@extends('app-front')

@section('content')
<div class="container-fluid container-login">
	<div class="row">

	@include('common.innernav')

		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('flash_message'))
	      <div class="alert alert-success abs">{{ session('flash_message') }}</div>
	    @endif

		@if (session('error_message'))
	      <div class="alert alert-danger abs">{{ session('error_message') }}</div>
	    @endif

	    <div class="page-title text-center">
	    	<h1 class="text-center login">Log in</h1>
	    </div>

	    <div class="login-form text-center">

			<form class="form-horizontal" role="form" method="POST" action="{{ url('/loginme') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="EMAIL ADDRESS">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="WACHTWOORD">
				</div>

				<button type="submit" class="btn btn-info log-in-submit icon"><i class="glyphicon glyphicon-user"></i>LOG IN</button>

			</form>

			<a href="{{ url('facebook') }}" class="btn btn-info btn-facebook icon"><i class="fa fa-facebook" aria-hidden="true"></i>LOG IN MET FACEBOOK</a>

    		<p class="login-link two"><a href="{{ url('/password/email') }}">Watchwoord vergeten</a></p>

			<p class="login-link geen">Nog geen account? <a href="{{ url('/register') }}">Meld je nu aan!</a></p>


	    </div> <!-- /.login-form text-center -->

	</div>

</div>
@endsection
