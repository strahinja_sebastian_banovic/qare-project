@extends('app-front')

@section('content')

<div class="container">

    <div class="row">
        <div class="page-title text-center">
            <h1 class="page-title zoekenact">Search for profile</h1>
        </div>

            <div class="edit-form">
                <form method="get" action="{{ url('profiles') }}" id="search-form" class="search-form zoekenactform">

                    <div class="form-group">
                        <input type="text" id="autocomplete" name="location" value="{{ old('location') }}" onFocus="geolocate()" class="form-control" placeholder="Search for city or zip code">
                        <input type="hidden" name="latlon" value="{{ old('latlon') }}" id="longitude">
                        <input type="hidden" name="city" value="{{ old('city') }}" id="city">
                    </div> <!-- /.form-group -->
					<p class="kies">KIES DE ADSTAND</p>
                    <div class="form-group kiesafstand profsearch">
                        <span class="kmlimit">500 KM</span>
                        <input id="distance" type="text" name="distance" value="{{ old('distance') }}" data-slider-handle="round" data-slider-tooltip="always">
                    </div> <!-- /.form-group -->

                    <div class="form-group">
                        <div class="col-md-12">
                            <p class="kies">SELECTEER TAGS</p>
                            <select data-placeholder="Choose interest tags..." class="chosen-select form-control" multiple name="tags[]" tabindex="4">
                                <option value=""></option>
                                @if(count($tags))
                                    @foreach($tags as $tag)
                                        <?php
                                            $tagname = DB::table('envtags')->where('qTagOID', '=', $tag->qTagOID)->first();
                                        ?>
                                        <option value="{{ $tagname->qTagOID }}">{{ $tagname->qTagDesc }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div style="padding-top:20px;"></div>

                    <p class="kies">PREFERENCES</p>
                    
                    <div class="row custom-checkbox customcb">

                        <div class>
                            <input type="checkbox" id="malesearch" name="male" value="1" checked="checked">
                            <label for="malesearch"><span></span> Man</label>
                        </div>
                        <div>
                            <input type="checkbox" id="femalesearch" class="checkbox-inline" name="female" value="2">
                            <label for="femalesearch"><span></span> Woman</label>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info btn-premium btn-block">SEARCH</button>
                        </div>
                    </div>

                </form>
            </div> <!-- /.edit-form -->
			<div style="padding-bottom: 100px;"></div>
    </div>

</div>

@endsection

@section('tailscripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&libraries=geometry,places&callback=initAutocomplete&language=en" async defer></script>
<script>

    var slider = new Slider("#distance", {
        step: 5,
        min: 0,
        max: 500,
        formatter: function(value) {
            return value + ' KM';
          },
          tooltip_position: 'top'
    });


    var placeSearch, autocomplete;

    function initAutocomplete() {

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types: ['geocode']});

        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        $("#longitude").val(place.geometry.location.lat() + ', ' + place.geometry.location.lng());


        for (var i = 0; i < place.address_components.length; i++) {

            var addressType = place.address_components[i].types[0];

            if(addressType == 'locality')
            {
                $("#city").val(place.address_components[i].long_name);
            }

        }
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>

<script type="text/javascript">
    $(".chosen-select").chosen({
        no_results_text: "Oops, nothing found!",
        max_selected_options: 5
    });
</script>
@endsection
