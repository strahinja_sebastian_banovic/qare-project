@extends('app-front')

@section('content')

<div class="container containerchat">


	<div class="row">
		<div class="col-md-12 text-center">
			<h1 class="page-title chat">Chat with {{ $user->qNameFirst.' '.$user->qNameLast }}</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			<div class="row">
				<div class=" " id="chat-window">
					@if(count($allchat) > 0)

					@foreach($allchat as $message)
					<?php
					if($message->qProfOIDReceiver == Auth::user()->qProfOID)
					{
						$align = "left";
						$senduser = DB::table('profiles')->where('qProfOID', '=', $message->qProfOIDSender)->first();
						$receiveruser = Auth::user();
					} else {
						$align = "right";
						$receiveruser = DB::table('profiles')->where('qProfOID', '=', $message->qProfOIDSender)->first();
						$senduser = Auth::user();
					}
					?>
					@if($align == 'left')
					<div class="inbox message firstuser">
						<div class="image">
							@if($senduser->qPicture)
								<img src="{{ url('images/users/thumbs/'.$senduser->qPicture) }}">
							@else
								@if($senduser->qGender==0 || $senduser->qGender==1)
									<img src="{{ url('img/user-male.png') }}">
								@else
									<img src="{{ url('img/user-female.png') }}">
								@endif
							@endif
						</div>
						<div class="messagebrefirst">
							<p>{{ $message->qMessage }}</p>
						</div>
					</div>
					<div class="clear"></div>
					@else
					<div class="inbox message seconduser">
						
						<div class="image">
							@if($senduser->qPicture)
								<img src="{{ url('images/users/thumbs/'.$senduser->qPicture) }}">
							@else
								@if($senduser->qGender==0 || $senduser->qGender==1)
									<img src="{{ url('img/user-male.png') }}">
								@else
									<img src="{{ url('img/user-female.png') }}">
								@endif
							@endif
						</div>
						<div class="messagebresecond">
							<p>{{ $message->qMessage }}</p>
						</div>
					</div>
					<div class="clear"></div>
					@endif
					@endforeach

					@endif
				</div>
			</div>

			<br>


			<form method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="row">
					<div class="col-md-12 text-center">
						<textarea name="message" id="message" class="form-control" placeholder="Type your message here..."></textarea>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6 col-md-offset-2 text-center dashboard">
					<input type="submit" id="send_button" class="btn btn-block btn-primary" value="Send">
					</div>
				</div>
			</form>

		</div>
	</div>
</div>

@endsection

@section('tailscripts')

<script type="text/javascript">
	$(document).ready(function() {

		pullData();

		$("#message").keypress(function(e) {
			if(e.which == 13) {
				e.preventDefault();
				sendMessage();
			}
		});

		$("#send_button").click(function(e) {
			e.preventDefault();
			sendMessage();
		});

	});

	function pullData()
	{
		retrieveChatMessages();
		setTimeout(pullData,3000);
	}

	function retrieveChatMessages()
	{
		$.ajax({
			url: '{{ url('messages/'.$user->qProfOID.'/retrieveChatMessages') }}',
			type: 'post',
			headers:
			{
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
			data: {
				'user'   : {{ $user->qProfOID }}
			},
			success: function(response) {
				if(response.length > 0){
					$("#chat-window").append(response);
				}
			}
		});
	}

	function sendMessage()
	{
		<?php
		if(Auth::user()->qPicture != ''){
			$image = '<img src="'.url('images/users/thumbs/'.Auth::user()->qPicture).'">';
		}
		else {
			if(Auth::user()->qGender==0 || Auth::user()->qGender==1){
				$image = '<img src="'.url('img/user-male.png').'">';
			}
			else {
				$image = '<img src="'.url('img/user-female.png').'">';
			}

		}

		?>
		var message = $.trim($("#message").val());

		if(message.length > 0)
		{
			$.ajax({
				url: '{{ url('messages/'.$user->qProfOID.'/sendmessage') }}',
				type: 'post',
				headers:
				{
					'X-CSRF-Token': $('input[name="_token"]').val()
				},
				data: {
					'message' : message,
					'user'   : {{ $user->qProfOID }}
				},
				success: function(response) {
					$("#chat-window").append('<div class="row inbox message"><div class="col-md-8 col-xs-10 text-right"><p>'+ response +'</p></div><div class="col-md-4 col-xs-2 image">{!! $image !!}</div></div>');
					$("#message").val('');
				}
			});
		} else if($("#message").val().length > 0) {
			$("#message").val('');
		}

	}
</script>

@endsection
