@extends('app-front')

@section('content')

<div class="container acts">
	<div class="row">

			<div class="acttop">

				<a class="actsearch"  href="{{ url('activities-search') }}">
					<i class="btn glyphicon glyphicon-search"></i> ZOEK OPROEP
				</a>
				<h1 class="page-title acttoph1">@if($page == 'activities') Oproepen @else Mijn Oproepen @endif</h1>

				<a class="actadd" href="{{ url('add-activity') }}"><i class="glyphicon glyphicon-plus"></i> MAAK OPROEP</a>

				<form method="get" class="actform">
					<div class="input-group actdate">
					  <span class="input-group-addon" id="sizing-addon2"><i class="glyphicon glyphicon-calendar"></i></span>
					  <input type="text" name="date" class="form-control date" aria-describedby="sizing-addon2" value="@if(Input::get('date')) {{ date('d.m.Y.', strtotime(Input::get('date'))) }} @else {{ date('d.m.Y') }} @endif" onchange="this.form.submit()">
					</div>
				</form>

			</div> <!-- /.acttop -->

			<div class="clear"></div>
			@if(count($activities) > 0)

			@foreach($activities as $activity)

			<div class="row activity">

				<div class="act">

					<div class="actimage" style="background:url({{ url('images/actimg.jpg') }});background-size:cover;">
						<div class="actimgbg"></div>
						<div class="acttags">
							<?php 
							$tags = DB::table('activtags')
							        ->where('qActivOID', '=', $activity->qActivOID)
							        ->get();
							 ?>
							 @if(count($tags) >0)
									@foreach($tags as $tag)
									<?php
										$qtag = DB::table('envtags')->where('qTagOID', '=', $tag->qTagOID)->where('qIsActive', '=', 1)->first();
									?>
									<p>{{ $qtag->qTagDesc }}</p>
									@endforeach
							@endif
						</div> <!-- /.acttags -->
						
						<?php 
						$checkActivFav = DB::table('activfavorites')->where('profile_id', Auth::user()->qProfOID)->where('activity_id', $activity->qActivOID)->first();
						?>
						
						<div class="activfavdiv">
							@if (empty($checkActivFav))
							<a href="#" class="makeactfav" data-actid="{{ $activity->qActivOID }}"><i class="glyphicon glyphicon-star-empty"></i> MAAK FAVORIET</a>
							@else
							<p class="activfav"><i class="glyphicon glyphicon-star"></i> FAVORIET</p>
							@endif
						</div>

					</div> <!-- /.actimage -->

					<div class="actinner">

						<div class="userinfo">
							<?php
								$user = App\User::find($activity->qProfOIDCreated);

								$year = substr($user->qDateOfBirth ,0 ,4);
								if($year == '0000')
								{
									$year = 'Not set.';
								} else {
									$datetime1 = date_create(date('Y-m-d'));
									$datetime2 = date_create(date('Y-m-d', strtotime($user->qDateOfBirth)));
									$interval = date_diff($datetime1, $datetime2);
									$year = $interval->y;
								}

								 $commentCount = DB::table('activcomments')->where('qProfOIDActivCreated', $user->qProfOID)->count();
							?>
						    @if($user->qPicture)
								<img class="img-rounded" src="{{ url('images/users/thumbs/'.$user->qPicture) }}">
							@else
								@if($user->qGender==0 || $user->qGender==1)
									<img class="img-rounded" src="{{ url('img/user-male.png') }}">
								@else
									<img class="img-rounded" src="{{ url('img/user-female.png') }}">
								@endif
							@endif

							<div class="namereviews">
								<p class="actname">{{ $user->qNameFirst }}, {{ $year }}</p>
								<p class="actreviews">{{ $commentCount }} Reviews</p>
								<p class="link"><a href="{{ url('view-profile') }}/{{ $user->qProfOID }}">></a></p>
							</div> <!-- /.namereviews -->


							<div class="clear"></div>

						</div> <!-- /.userinfo -->

						<div class="actwat">
						    <div class="waar">
						    	<h4>WAAR</h4>
						        <?php 

						        if($activity->qLocation != ''){
								$place = explode(' | ', $activity->qLocation);
								}
						         ?>
						         <p>{{ $place[1] }}<p>

						    </div> <!-- /.waar -->
						    <div class="datum">
						    	<h4>DATUM EN TUD</h4>
						    	<p>{{ date('d M Y', strtotime($activity->qDate)).' '.substr($activity->qTime, 0, -3) }}</p>
						    </div>

						    <div class="duration">
						    	<h4>DURATION</h4>
						        <p>{{ $activity->qDuration }} hours</p>
						    </div> <!-- /.duration -->
						    <div class="clear"></div>
						</div> <!-- /.actwat -->

						<div class="actdesc">
							{{ $activity->qDescription }}
						</div>

						<?php
							$checkifbooked = DB::table('activbookings')
								->where('qActivOID', '=', $activity->qActivOID)
								->where('qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
								->first();
						?>

						@if($checkifbooked)
							<p class="user-details-success"><span class="glyphicon glyphicon-ok"></span> ACTIVITEIT GEACCEPTEERD</p>
						@elseif($activity->qProfOIDCreated != Auth::user()->qProfOID)
						<a class="favorite-btn btn-block" href="{{ url('book-activity/'.$activity->qActivOID) }}" onclick="return confirm('Are you sure you want to book this activity?');"><span class="glyphicon glyphicon-ok-circle"></span> ACTIVITEIT AANGEVRAAGD</a>
						@endif
					
					</div> <!-- /.actinner -->

				</div> <!-- /.act -->

			</div>
			@endforeach

			{!! str_replace('/?', '?', $activities->appends(Input::only('location', 'distance', 'tags', 'home', 'public', 'inside', 'outside', 'sdate', 'days'))->render()) !!}

			@else
			<h3 class="page-body-title">No activities at this moment...</h3>
			@endif


			@if($page == 'myactivities')

			@else
				<div class="row bottom-buttons">
					<div class="col-md-4 col-xs-9">
						<p><a href="{{ url('my-activities') }}">Go to my activities</a></p>
					</div>
				</div>

			@endif

	</div>
</div>

@endsection
