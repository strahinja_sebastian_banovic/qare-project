@extends('app')

@section('content')

<div class="container">

  <div class="row">
    <div class="col-xs-10 col-xs-offset-1">
      @if (session('flash_message'))
      <div class="alert alert-success">{{ session('flash_message') }}</div>
      @endif
      @if (session('error_message'))
      <div class="alert alert-danger">{{ session('error_message') }}</div>
      @endif
    </div>
  </div>


  <div class="row">
    <form method="post">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="col-xs-4 col-xs-offset-1">
        <label for="statistics">Statistics</label>
        <select name="statistics" id="statistictype" class="form-control">
          <option value="">Select statistic</option>
          <option value="noda" @if(Input::get('statistics')=='noda') {{ 'selected="selected"' }} @endif>Number deleted accounts</option>
          <option value="hmthub" @if(Input::get('statistics')=='hmthub') {{ 'selected="selected"' }} @endif>How many times has a user booked</option>
          <option value="wuhmb" @if(Input::get('statistics')=='wuhmb') {{ 'selected="selected"' }} @endif>Which user has the most bookings</option>
          <option value="wabm" @if(Input::get('statistics')=='wabm') {{ 'selected="selected"' }} @endif>Which activities are booked the most</option>
          <option value="waabb" @if(Input::get('statistics')=='waabb') {{ 'selected="selected"' }} @endif>When are activities been booked</option>
          <option value="waabwa" @if(Input::get('statistics')=='waabwa') {{ 'selected="selected"' }} @endif>Which ages are booking what activities</option>
          <option value="wamwwa" @if(Input::get('statistics')=='wamwwa') {{ 'selected="selected"' }} @endif>Which age matches with what activity</option>
          <option value="dmbmw" @if(Input::get('statistics')=='dmbmw') {{ 'selected="selected"' }} @endif>Different matches between man/woman</option>
          <option value="mpt" @if(Input::get('statistics')=='mpt') {{ 'selected="selected"' }} @endif>Most popular tags</option>
          <option value="oc" @if(Input::get('statistics')=='oc') {{ 'selected="selected"' }} @endif>Total chat requests</option>
          <option value="cpu" @if(Input::get('statistics')=='cpu') {{ 'selected="selected"' }} @endif>Chat per person</option>
          <option value="srfu" @if(Input::get('statistics')=='srfu') {{ 'selected="selected"' }} @endif>See reaction from users to users</option>
          <option value="mfsis" @if(Input::get('statistics')=='mfsis') {{ 'selected="selected"' }} @endif>Most frequent searches in search</option>
          <option value="hmtabbaod" @if(Input::get('statistics')=='hmtabbaod') {{ 'selected="selected"' }} @endif>How many times are bookings being accepted or declined</option>
        </select>
      </div>

      <div class="col-xs-3" id="userselect" style="display: none;">
        <label for="user">View statistics for user</label>
        <select name="user" class="form-control">
          <option value="">Select user</option>
          @foreach($profiles as $profile)
          <option value="{{ $profile->qProfOID }}" @if($profile->qProfOID == Input::get('user')) {{ 'selected="selected"' }} @endif>{{ $profile->qNameFirst.' '.$profile->qNameLast }}</option>
          @endforeach
        </select>
      </div>

      <div class="col-xs-3" id="activityselect" style="display: none;">
        <label for="user">View statistics for activity</label>
        <select name="activity" class="form-control">
          <option value="">Select activity</option>
          @foreach($activities as $activity)
          <option value="{{ $activity->qActivOID }}" @if($activity->qActivOID == Input::get('activity')) {{ 'selected="selected"' }} @endif>{{ $activity->qTitle }}</option>
          @endforeach
        </select>
      </div>

      <div class="col-xs-3" id="agesselect" style="display: none;">
        <label for="user">View statistics by user ages</label>
        <select name="ages" class="form-control">
          <option value="">Select age</option>
          <option value="under18" @if(Input::get('ages')=='under18') {{ 'selected="selected"' }} @endif>{{ 'Under 18' }}</option>
          <option value="18-30" @if(Input::get('ages')=='18-30') {{ 'selected="selected"' }} @endif>{{ '18 - 30' }}</option>
          <option value="30-40" @if(Input::get('ages')=='30-40') {{ 'selected="selected"' }} @endif>{{ '30 - 40' }}</option>
          <option value="40-50" @if(Input::get('ages')=='40-50') {{ 'selected="selected"' }} @endif>{{ '40 - 50' }}</option>
          <option value="over50" @if(Input::get('ages')=='over50') {{ 'selected="selected"' }} @endif>{{ 'Over 50' }}</option>
        </select>
      </div>


      <div class="col-xs-2 col-xs-offset-1">
        <label for="user">&nbsp;</label><br>
        <input type="submit" class="btn btn-primary" value="Search">
      </div>
    </form>
  </div>

  <hr>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Statistics</div>

        <div class="panel-body">



          @if(Input::get('statistics')=='hmthub' && Input::get('user'))
          <p>User <b>{{ $user->qNameFirst.' '.$user->qNameLast }}</b> has been booked <b>{{ $bookings }}</b> times.</p>
          <p>This profile has been viewed <b>{{ $profileview }}</b> times.
            @if(count($ifuserchosesalwayssame)==1)
            <p>This user every time chooses same user <b></b> for activity</p>
            @endif
            @endif



            @if(Input::get('statistics')=='dmbmw' )
              <h3>Different matches between man/woman</h3>
              <table class="table table-striped">
              <tr>
                <th>Man/Man</th>
                <th>Woman/Woman</th>
                <th>Man/Woman (Woman/Man)</th>
              </tr>
              <tr>
                <td>{{ $manman }}</td>
                <td>{{ $womanwoman }}</td>
                <td>{{ $manwoman }}</td>
              </tr>
              </table>

              <div id="pie_chart_matches"></div>
            @endif




            @if(Input::get('statistics')=='mfsis')
            <h3>Most frequent search for profile</h3>
            @if(count($profsearch) > 0)
            <table class="table table-striped">
              <tr>
                <th>Location</th>
                <th>Distance</th>
                <th>Man</th>
                <th>Woman</th>
                <th>Tags</th>
                <th>Number of searches</th>
              </tr>
              @foreach($profsearch as $prof)
              <tr>
                <td>{{ $prof->qLocation }}</td>
                <td>{{ $prof->qDistance }}</td>
                <td> @if($prof->qIsMale == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                <td> @if($prof->qIsFemale == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                <?php
                $tagforshow = '';
                if($prof->qTags != '')
                {
                  $tags = substr($prof->qTags, 0, -1);
                  $tags = substr($tags, 1);
                  $tags = explode('][', $tags);

                  foreach ($tags as $tag) {
                    $tagdesc = DB::table('envtags')->where('qTagOID', '=', $tag)->first();

                    $tagforshow = $tagdesc->qTagDesc.', ';
                  }

                  $tagforshow = substr($tagforshow, 0, -2);
                }
                ?>
                <td>{{ $tagforshow }}</td>
                <td>{{ $prof->qCounter }}</td>
              </tr>
              @endforeach
            </table>
            @endif

            @if(count($activsearch) > 0)
            <h3>Most frequent search for activities</h3>

            @if($favourite)
            <p>{{ $favourite }}</p>
            @endif

            @if(count($activsearch) > 0)
            <table class="table table-striped">
              <tr>
                <th>Location</th>
                <th>Distance</th>
                <th>Home</th>
                <th>Public</th>
                <th>Inside</th>
                <th>Outside</th>
                <th>Days</th>
                <th>Tags</th>
                <th>Number of searches</th>
              </tr>
              @foreach($activsearch as $activ)
              <tr>
                <td>{{ $activ->qLocation }}</td>
                <td>{{ $activ->qDistance }}</td>
                <td> @if($activ->qIsHome == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                <td> @if($activ->qIsPublic == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                <td> @if($activ->qIsInside == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                <td> @if($activ->qIsOutside == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                <?php
                $tagforshow = '';
                $daysforshow = '';

                if($activ->qDays != '')
                {
                  $days = substr($activ->qDays, 0, -1);
                  $days = substr($days, 1);
                  $days = explode('][', $days);

                  foreach ($days as $day) {
                    $daysforshow .= date('l', strtotime("Sunday +{$day} days")).', ';
                  }

                  $daysforshow = substr($daysforshow, 0, -2);
                }

                if($activ->qTags != '')
                {
                  $tags = substr($activ->qTags, 0, -1);
                  $tags = substr($tags, 1);
                  $tags = explode('][', $tags);

                  foreach ($tags as $tag) {
                    $tagdesc = DB::table('envtags')->where('qTagOID', '=', $tag)->first();

                    $tagforshow .= $tagdesc->qTagDesc.', ';
                  }

                  $tagforshow = substr($tagforshow, 0, -2);
                }
                ?>
                <td>{{ $daysforshow }}</td>
                <td>{{ $tagforshow }}</td>
                <td>{{ $activ->qCounter }}</td>
              </tr>
              @endforeach
              @endif
              @endif


              @endif




              @if(Input::get('statistics')=='oc')
              <table class="table table-striped">
                <tr>
                  <th>Chat requests</th>
                  <td> {{ $chatrequests }}</td>
                </tr>
                <tr>
                  <th>Pending chat requests</th>
                  <td> {{ $chatrequestspending }} </td>
                </tr>
                <tr>
                  <th>Accepted chat requests</th>
                  <td> {{ $chatrequestsaccepted }} </td>
                </tr>
                <tr>
                  <th>Declined chat requests</th>
                  <td> {{ $chatrequestsdeclined }} </td>
                </tr>
              </table>
              @endif




              @if(Input::get('statistics')=='cpu' && Input::get('user'))
              <div class="row">
                <div class="col-md-12">
                  <p>This profile has been viewed <b>{{ $profileview }}</b> times.</p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <h3>{{ $currentuser->qNameFirst.' '.$currentuser->qNameLast }}</h3>
                </div>
                <div class="col-md-6">
                  <h3>Sent requests</h3>
                  <table class="table table-striped">
                    <tr>
                      <th>Chat requests</th>
                      <td> {{ $sendchatrequests }}</td>
                    </tr>
                    <tr>
                      <th>Pending chat requests</th>
                      <td> {{ $sendchatrequestspending }} </td>
                    </tr>
                    <tr>
                      <th>Accepted chat requests</th>
                      <td> {{ $sendchatrequestsaccepted }} </td>
                    </tr>
                    <tr>
                      <th>Declined chat requests</th>
                      <td> {{ $sendchatrequestsdeclined }} </td>
                    </tr>
                  </table>
                </div>

                <div class="col-md-6">
                  <h3>Received requests</h3>
                  <table class="table table-striped">
                    <tr>
                      <th>Chat requests</th>
                      <td> {{ $receivedchatrequests }}</td>
                    </tr>
                    <tr>
                      <th>Pending chat requests</th>
                      <td> {{ $receivedchatrequestspending }} </td>
                    </tr>
                    <tr>
                      <th>Accepted chat requests</th>
                      <td> {{ $receivedchatrequestsaccepted }} </td>
                    </tr>
                    <tr>
                      <th>Declined chat requests</th>
                      <td> {{ $receivedchatrequestsdeclined }} </td>
                    </tr>
                  </table>
                </div>
              </div>
              @endif





              @if(Input::get('statistics')=='srfu' && Input::get('user'))
              <div class="row">
                <div class="col-md-12">
                  <h3>{{ $currentuser->qNameFirst.' '.$currentuser->qNameLast }} chats</h3>
                  <p>This profile has been viewed <b>{{ $profileview }}</b> times.</p>
                </div>
                <div class="col-md-12">
                  @if(count($allchats) > 0)
                  <table class="table table-striped">
                    <tr>
                      <th>Chat with</th>
                      <th>Who sent request</th>
                      <th>View chat</th>
                    </tr>
                    @foreach($allchats as $chat)
                    <?php

                    $chatrequest = DB::table('chat')->where('qChatOID', '=', $chat->qChatOID)->first();

                    if($chatrequest->qProfOIDRequested == Input::get('user'))
                    {
                      $sendrequestuser = $currentuser;

                      $otheruser = DB::table('profiles')->where('qProfOID', '=', $chatrequest->qProfOID)->first();

                    } else {
                      $sendrequestuser = DB::table('profiles')->where('qProfOID', '=', $chatrequest->qProfOIDRequested)->first();

                      $otheruser = $sendrequestuser;
                    }
                    ?>
                    <tr>
                      <td> {{ $otheruser->qNameFirst.' '.$otheruser->qNameLast }} </td>
                      <td>{{ $sendrequestuser->qNameFirst.' '.$sendrequestuser->qNameLast }}</td>
                      <td>
                        <a class="btn btn-info" target="_blank" href="{{ url('admin/chat/'.$chat->qChatOID.'/'.$currentuser->qProfOID) }}">View</a>
                      </td>
                    </tr>
                    @endforeach
                  </table>
                  @else
                  <h3>User doesn't have any chats at the moment</h3>
                  @endif
                </div>
              </div>

              @endif





              @if(Input::get('statistics')=='mpt')
              <h3>10 Most popular tags</h3>
              <table class="table table-striped">
                <tr>
                  <th>Tag</th>
                  <th>Number of tag</th>
                </tr>
                @foreach($populartags as $popular)
                <tr>
                  <td>{{ DB::table('envtags')->where('qTagOID', '=', $popular->qTagOID)->first()->qTagDesc }}</td>
                  <td>{{ $popular->counter }}</td>
                </tr>
                @endforeach
              </table>

              <hr>

              <h3>10 Most unpopular tags</h3>
              <table class="table table-striped">
                <tr>
                  <th>Tag</th>
                  <th>Number of tag</th>
                </tr>
                @foreach($unpopulartags as $unpopular)
                <tr>
                  <td>{{ DB::table('envtags')->where('qTagOID', '=', $unpopular->qTagOID)->first()->qTagDesc }}</td>
                  <td>{{ $unpopular->counter }}</td>
                </tr>
                @endforeach
              </table>
              @endif



              @if(Input::get('statistics')=='wuhmb')
              <p>User <b>{{ $user->qNameFirst.' '.$user->qNameLast }}</b> has the most bookings <b>({{ $mostbooking->counter }})</b>.</p>
              @endif




              @if(Input::get('statistics')=='wamwwa' && Input::get('activity'))
              <h3>Bookings by ages for: {{ $singleactivity->qTitle }} </h3>
              <table class="table table-striped">
                <tr>
                  <th>Under 18 years</th>
                  <th>18-30 years</th>
                  <th>30-40 years</th>
                  <th>40-50 years</th>
                  <th>Over 50 years</th>
                </tr>
                <tr>
                  <td>{{ $bookings18 }}</td>
                  <td>{{ $bookings30 }}</td>
                  <td>{{ $bookings40 }}</td>
                  <td>{{ $bookings50 }}</td>
                  <td>{{ $bookingsover50 }}</td>
                </tr>
              </table>

              <div id="pie_chart_div"></div>
              @endif



              @if(Input::get('statistics')=='hmtabbaod')
              <h3>How many times are bookings being accepted or declined</h3>
              <p>Number of accepted bookings: <b>{{ $acceptedbookings }}</b>.</p>
              <p>Number of declined bookings: <b>{{ $declinedbookings }}</b>.</p>
              @endif







              @if(Input::get('statistics')=='waabwa' && Input::get('ages'))
              <?php
              if(Input::get('ages')=='under18')
              {
                $title = 'having 18 or less years';
              }
              else if(Input::get('ages')=='18-30')
              {
                $title = 'having 18 - 30 years';
              }
              else if(Input::get('ages')=='30-40')
              {
                $title = 'having 30 - 40 years';
              }
              else if(Input::get('ages')=='40-50')
              {
                $title = 'having 40 - 50 years';
              }
              else if(Input::get('ages')=='over50')
              {
                $title = 'having over 50 years';
              }
              ?>
              <h3>10 Most booked activities by users {{ $title }}</h3>
              <table class="table table-striped">
                <tr>
                  <th>Activity</th>
                  <th>Created by</th>
                  <th>Booked</th>
                </tr>
                @foreach($ages as $age)
                <?php
                $activity = App\AdminActivity::find($age->qActivOID);
                $useract =  App\User::find($activity->qProfOIDCreated);
                ?>
                <tr>
                  <td>{{ $activity->qTitle }}</td>
                  <td>{{ $useract->qNameFirst.' '.$useract->qNameLast }}</td>
                  <td>{{ $age->counter }}</td>
                </tr>
                @endforeach
              </table>
              @endif





              @if(Input::get('statistics')=='noda')
              <p>Number of deleted accounts <b>{{ count($deletedaccounts) }}</b>.</p>
              @if(count($deletedaccounts) >0)
              <table class="table table-striped">
                <tr>
                  <th>User</th>
                  <th>Email</th>
                  <th>Created</th>
                </tr>
                @foreach($deletedaccounts as $users)
                <tr>
                  <td>
                    <div>{{ $users->qNameFirst.' '.$users->qNameLast }}</div>
                  </td>
                  <td>
                    <div>{{ $users->qEmail }}</div>
                  </td>
                  <td>
                   <div>{{ date('Y-m-d', strtotime($users->qCreatedAt)) }}</div>
                 </td>
               </tr>
               @endforeach
             </table>
             @endif
             @endif





             @if(Input::get('statistics')=='waabb' && Input::get('activity'))
             <h3>Activity: {{ $singleactivity->qTitle }}</h3>
             <?php
             $useract = App\User::find($singleactivity->qProfOIDCreated)
             ?>
             <h3>Created by: {{ $useract->qNameFirst.' '.$useract->qNameLast }}</h3>
             <table class="table table-striped">
              <tr>
                <th>Booked by</th>
                <th>Booking date</th>
              </tr>
              @foreach($allbookings as $allbooking)
              <?php
              $userbooked =  App\User::find($allbooking->qProfOIDBookingCreated);
              ?>
              <tr>
                <td>{{ $userbooked->qNameFirst.' '.$userbooked->qNameLast }}</td>
                <td>{{ date('d.m.Y. H:i', strtotime($allbooking->qCreatedAt)) }}</td>
              </tr>
              @endforeach
            </table>
            @endif






            @if(Input::get('statistics')=='wabm')

            <h3>10 Most booked activities </h3>
            <table class="table table-striped">
              <tr>
                <th>Activity</th>
                <th>Created by</th>
                <th>Booked</th>
              </tr>
              @foreach($mosttenbookings as $mostten)
              <?php
              $activity = App\AdminActivity::find($mostten->qActivOID);
              $useract =  App\User::find($activity->qProfOIDCreated);
              ?>
              <tr>
                <td>{{ $activity->qTitle }}</td>
                <td>{{ $useract->qNameFirst.' '.$useract->qNameLast }}</td>
                <td>{{ $mostten->counter }}</td>
              </tr>
              @endforeach
            </table>
            @endif



            @if(isset($user))
            <table class="table table-striped">
              <tr>
                <th>User</th>
                <td>{{ $user->qNameFirst.' '.$user->qNameLast }}</td>
              </tr>
              <tr>
                <th>Total activities</th>
                <td>{{ $useractivities }}</td>
              </tr>
              <tr>
                <th>Total bookings</th>
                <td>{{ $userbookings }}</td>
              </tr>
            </table>

            <div id="chart_div"></div>
            @endif

          </div>

        </div>
      </div>
    </div>
  </div>
  @endsection

  @section('tailscripts')

  <script type="text/javascript">
    $(document).ready(function() {

      var stattype = $("#statistictype").val();
      if(stattype == 'hmthub' || stattype == 'cpu' || stattype == 'srfu')
      {
        $("#userselect").show();
        $("#activityselect").hide();
        $("#agesselect").hide();
        $("#userselect").find('select').removeAttr('disabled');
        $("#activityselect").find('select').attr('disabled', 'disabled');
        $("#agesselect").find('select').attr('disabled', 'disabled');
      }
      else if(stattype == 'waabb' || stattype == 'wamwwa') {
        $("#userselect").hide();
        $("#userselect").find('select').attr('disabled', 'disabled');
        $("#activityselect").show();
        $("#activityselect").find('select').removeAttr('disabled');
        $("#agesselect").hide();
        $("#agesselect").find('select').attr('disabled', 'disabled');
      }
      else if(stattype == 'waabwa')
      {
        $("#userselect").hide();
        $("#activityselect").hide();
        $("#agesselect").show();
        $("#agesselect").find('select').removeAttr('disabled');
        $("#activityselect").find('select').attr('disabled', 'disabled');
        $("#userselect").find('select').attr('disabled', 'disabled');
      }
      else {
        $("#userselect").hide();
        $("#userselect").find('select').attr('disabled', 'disabled');
        $("#activityselect").hide();
        $("#activityselect").find('select').attr('disabled', 'disabled');
        $("#agesselect").hide();
        $("#agesselect").find('select').attr('disabled', 'disabled');
      }

      $( "#statistictype" ).change(function() {
        var stattype = $(this).val();
        if(stattype == 'hmthub' || stattype == 'cpu' || stattype == 'srfu')
        {
          $("#userselect").show();
          $("#activityselect").hide();
          $("#agesselect").hide();
          $("#userselect").find('select').removeAttr('disabled');
          $("#activityselect").find('select').attr('disabled', 'disabled');
          $("#agesselect").find('select').attr('disabled', 'disabled');
        }
        else if(stattype == 'waabb' || stattype == 'wamwwa') {
          $("#userselect").hide();
          $("#userselect").find('select').attr('disabled', 'disabled');
          $("#activityselect").show();
          $("#activityselect").find('select').removeAttr('disabled');
          $("#agesselect").hide();
          $("#agesselect").find('select').attr('disabled', 'disabled');
        }
        else if(stattype == 'waabwa')
        {
          $("#userselect").hide();
          $("#activityselect").hide();
          $("#agesselect").show();
          $("#agesselect").find('select').removeAttr('disabled');
          $("#activityselect").find('select').attr('disabled', 'disabled');
          $("#userselect").find('select').attr('disabled', 'disabled');
        }
        else {
          $("#userselect").hide();
          $("#userselect").find('select').attr('disabled', 'disabled');
          $("#activityselect").hide();
          $("#activityselect").find('select').attr('disabled', 'disabled');
          $("#agesselect").hide();
          $("#agesselect").find('select').attr('disabled', 'disabled');
        }
      });
    });
  </script>


  @if(Input::get('statistics') == 'dmbmw')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Matches', 'Number of matches'],
          ['Man/Man', {{ $manman }}],
          ['Woman/Woman', {{ $womanwoman }}],
          ['Man/Woman (Woman/Man)', {{ $manwoman }}]
          ]);

        var options = {
          title: 'Matches by gender'
        };

        var chart = new google.visualization.PieChart(document.getElementById('pie_chart_matches'));
        chart.draw(data, options);
      }
    </script>
  @endif

  @if(Input::get('statistics')=='wamwwa' && Input::get('activity'))
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Ages', 'Number of bookings'],
        ['Under 18', {{ $bookings18 }}],
        ['18 - 30', {{ $bookings30 }}],
        ['30 - 40', {{ $bookings40 }}],
        ['40 - 50', {{ $bookings50 }}],
        ['Over 50',{{ $bookingsover50 }}]
        ]);

      var options = {
        title: 'Bookings by ages',
        pieHole: 0.4,
      };

      var chart = new google.visualization.PieChart(document.getElementById('pie_chart_div'));
      chart.draw(data, options);
    }
  </script>
  @endif

  @if(isset($user))
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

      <?php
      $months = array();
      $monthsb = array();
      ?>
      @foreach($activitiesbymonth as $acbymonth)
      <?php
      $month = (int)(explode('-', $acbymonth->date)[1]);
      $months[$month] = $acbymonth->counter;
      ?>
      @endforeach

      @foreach($bookingsbymonth as $boobymonth)
      <?php
      $month = (int)(explode('-', $boobymonth->date)[1]);
      $monthsb[$month] = $boobymonth->counter;
      ?>
      @endforeach

      var data = google.visualization.arrayToDataTable([
       ['Months', 'Number of activities', 'Number of bookings'],
       @for($i=1; $i<13; $i++)
       ['{{ date('Y-'.$i) }}',
       @if(isset($months[$i])) {{ $months[$i] }}, @else {{ 0 }}, @endif
       @if(isset($monthsb[$i])) {{ $monthsb[$i] }}, @else {{ 0 }} @endif
       ],
       @endfor
       ]);

      var options = {
        title: 'Number of activities for @if(Input::get('year')){{ Input::get('year') }} @else {{ date('Y') }} @endif',
        hAxis: {
          title: 'Months'
        },
        vAxis: {
          title: 'Number of activities'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }
  </script>
  @endif

  @endsection
