@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <p>Just for testing</p>
					<p>email: qare-project@qare.com</p>
					<p>pass: 1234</p>

					@if(count($errors))
						@foreach($errors->all() as $error)
							<div class="error">
							{{ ($error) }}
							</div>
						@endforeach
					@endif

					@if(Session::has('error_message'))
						<div class="success">{{ Session::get('error_message') }}</div>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/loginme') }}">

						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" value="1"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

						<div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>Login
                                </button>

						<!--
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
						-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

