@extends('app')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<h3>Edit booking</h3>
		</div>
	</div>

	@if(count($errors))
	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{ ($error) }}</p>
				@endforeach
			</div>
		</div>
	</div>
	@endif

	<div class="row">
		<div class="col-md-6 col-md-offset-2">

			@if (session('error_message'))
			<div class="alert alert-danger">{{ session('error_message') }}</div>
			@endif

			<form method="post" enctype="multipart/form-data" action="{{ url('admin/booking-edit/'.$booking->qActivOID.'/'.$booking->qProfOIDBookingCreated) }}">


				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="activity">Booking for activity</label>
							<select name="activity" class="form-control">
								@foreach($activities as $activity)
								<option value="{{ $activity->qActivOID }}" @if($booking->qActivOID == $activity->qActivOID) {{ 'selected="selected"' }} @endif >{{ $activity->qTitle }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="title">Booking for user</label>
							<select name="profile" class="form-control">
								@foreach($users as $user)
								<option value="{{ $user->qProfOID }}" @if($booking->qProfOIDBookingCreated == $user->qProfOID) {{ 'selected="selected"' }} @endif >{{ $user->qNameFirst.' '.$user->qNameLast }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="active">State</label>
							<select name="state" class="form-control">
								<option value="0" @if($booking->qStatus == 0) {{ 'selected="selected"' }} @endif>Requested</option>
								<option value="1" @if($booking->qStatus == 1) {{ 'selected="selected"' }} @endif>Accepted</option>
								<option value="2" @if($booking->qStatus == 2) {{ 'selected="selected"' }} @endif>Declined</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<input type="submit" class="btn btn-block btn-primary" value="Save">
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

@endsection
