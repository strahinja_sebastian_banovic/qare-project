@extends('app-front')

@section('content')
<div class="container-fluid container-contact" id="start">
	<div class="row">
		@include('common.innernav')

		<div class="text-center contact-title">
          <h3>De app voor gezelligheid bij jou in de buurt!</h3>
          <div class="buttons-contact">
          	<a href="#howhetwerkt">
            	<button class="btn btn-default btn-hoe-het">HOE HET WERKT</button>
            </a>
            <a href="#inschrijven">
            	<button class="btn btn-default btn-aanmelden">AANMELDEN</button>
            </a>
          </div>
        </div>

        {{-- <div class="image-phone">
          <img src="{{ url('/') }}/img/mobile.png">
        </div> --}}
        </div>
</div>

      <div class="container container-white wiq">
        <div class="row">
          <div class="image-phone col-sm-6 col-xs-12">
              <img class="" src="{{ url('/') }}/img/mobile.png">
          </div>
          <div class="wat-qare-div col-sm-6 col-xs-12">
            <h2 class="blue-title">Wat is Qare?</h2>
            <p>Juist dat koffie, een korte wandeling door het park of een fijn gesprek, geeft de dag kleur. Ben jij deze gezellige buur of zoek jij een gezeling moment in je dag? <span class="orange-underline">Meld je dan nu aan!</span></p>
          </div>
        </div>
      </div>

      <div class="clear"></div>

      <div class="container-fluid graybg">

        <div class="row">

          <div class="hoe-werkt-div col-xs-12" id="howhetwerkt">
            <h2 class="blue-title">Hoe werkt qare?</h2>
            <p>Zoek en filter eenvoudig naar de activiteit die jou leuk lijkt om te doen. En help hiermee een persoon bij jou in de buurt door iets leuks te doen.</p>
          </div>
          
          <div class="four-icons col-xs-12">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="col-white">
                <img src="{{ url('/') }}/img/user.png">
                <h4>PERSOONLIJK</h4>
                <p>Zoek een leuke activiteit die bij jou past, bekijk het profiel, lees de leuke reacties van anderen en biedt een leuk moment van de tag.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="col-white">
                <img src="{{ url('/') }}/img/pin.png">
                <h4>LEKKER DICHTBIJ</h4>
                <p>Eenzaamheid is dichterbij dan je denkt zie de profielen en leuke activiteiten die je kunt doen op basis van je locatie.</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="col-white">
                <img src="{{ url('/') }}/img/calendar.png">
                <h4>PLAN HET ZELF</h4>
                <p>Ook zo'n drukke agenda? Bij Qare help je wanneer het you uitkomt!</p>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="col-white">
                <img src="{{ url('/') }}/img/coffe.png">
                <h4>NERGENS AAN VAAST</h4>
                <p>Geen zin om wekelijks of maandelijks koffie te drinken of te wandelen met een buurtgenoot? Ook als je spontaan iets goeds wilt doen, is Qare het platform voor jou.</p>
              </div>
            </div>
          </div>
          
            <div class="clear"></div>


        </div>

      </div>

      <div class="container container-white" id="video">
        <div class="row text-center video-div">
          <img src="{{ url('/') }}/img/video.png">
        </div>
      </div>

      <div class="container-fluid container-blue text-center srif" id="inschrijven">
        <h3 class="">Schrijf je in!</h3>
        <form class="form-inline contact-subscribe">
          <div class="form-group">
            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="VOER JE E-MAIL ADRES IN...">
          </div>
          <button type="submit" class="btn btn-info btn-premium">AANMELDEN</button>
        </form>
      </div>

      @if(count($errors))
          <div class="alert alert-danger">
            @foreach($errors->all() as $error)
              <p>{{ ($error) }}</p>
            @endforeach
          </div>
          @endif

          @if (session('flash_message'))
            <div class="alert alert-success">{{ session('flash_message') }}</div>
          @endif

          @if (session('error_message'))
            <div class="alert alert-danger">{{ session('error_message') }}</div>
          @endif

      <div class="container container-white" id="contact">
        <div class="row">
          <div class="col-sm-8 col-xs-12 form-contact-div">
            <h2 class="blue-title text-center">Contact</h2>

            <form method="post" action="{{ url('contact') }}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<input type="text" id="name" class="form-control" placeholder="NAAM" name="name" value="">
					</div>

					<div class="form-group">
						<input type="email" id="email" class="form-control" placeholder="E-MAIL ADRES" name="email" value="">
					</div>

					<div class="form-group">
						<input type="text" id="title" class="form-control" placeholder="ONDERWERP" name="title" value="">
					</div>
					<div class="form-group">
						<textarea id="message" class="form-control" rows="8" placeholder="BERICHT" name="message"></textarea>
					</div>

					<button type="submit" class="btn btn-info btn-block btn-grey">VERSTUUR</button>

			</form>

          </div>
        </div>
      </div>

      <div class="container-fluid graybg">
        <div class="social-icons text-center">
          <a class="fb" href="http://ec2-52-91-110-115.compute-1.amazonaws.com/qare/qare-design/contact.html#"><img src="{{ url('/') }}/img/facebook.png"></a>
          <a class="tw" href="http://ec2-52-91-110-115.compute-1.amazonaws.com/qare/qare-design/contact.html#"><img src="{{ url('/') }}/img/twitter.png"></a>
        </div>
      </div>

      <div class="container container-white text-center footer">
        <p class="copyright">Copyright © Qare, All rights reserved</p>
        <p class="copyright2">A <a href="#">G8Things</a> Production</p>
      </div>

	</div>
@endsection
