@extends('app-front')

@section('content')

<div class="container">
	
	<div class="row">
	    
		<div class="page-title text-center">
            <h1 class="profilesh1">Favorieten</h1>
        </div>

        <div class="search-form">

			<form method="get" class="searchfavorites">
				
				<i class="btn glyphicon glyphicon-search"></i>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<input type="text" name="name" placeholder="ZOEKEN">
			</form>
           	
			<div class="row favorite-row">
			    	
			    @if(count($profiles) > 0)

				@foreach($profiles as $profile)

				<?php
					$numofactivities = DB::table('activities')->where('qProfOIDCreated', '=', $profile->qProfOID)->whereRaw("CONCAT('qDate', ' ', 'qTime') >= '".date('Y-m-d H:i:s')."'")->count();
				?>

				<div class="col-md-6 col-sm-6 col-xs-12">

				    <div class="favorites-image">
				    	<a href="{{ url('view-profile/'.$profile->qProfOID) }}">
                  		@if($profile->qPicture)
							<img class="img-rounded" src="{{ url('images/users/thumbs/'.$profile->qPicture) }}">
						@else
							@if($profile->qGender==0 || $profile->qGender==1)
								<img class="img-rounded" src="{{ url('img/user-male.png') }}">
							@else
								<img class="img-rounded" src="{{ url('img/user-female.png') }}">
							@endif
						@endif
						</a>
	                    <div class="favorites-info">
	                    	<?php 
							$year = substr($profile->qDateOfBirth ,0 ,4);
							if($year == '0000')
							{
								$year = 'Not set.';
							} else {
								$datetime1 = date_create(date('Y-m-d'));
								$datetime2 = date_create(date('Y-m-d', strtotime($profile->qDateOfBirth)));
								$interval = date_diff($datetime1, $datetime2);
								$year = $interval->y;
							}
							?>
		                      <div class="favorites-name">{{ $profile->qNameFirst }}, {{ $year }}</div>
		                      <div class="favorites-place">{{ $profile->qCity }}</div>
		                      <?php 
			                    $commentCount = DB::table('activcomments')->where('qProfOIDActivCreated', $profile->qProfOID)->count();
			                    ?>
		                      <div class="favorites-reviews">{{ $commentCount }} reviews</div>
		                      {{-- <div class="favorites">{{ $numofactivities }} activities</div> --}}

		                      <div class="col-xs-2 text-center cancelfav">
									<a class="btn btn-info btn-xs" href="favorites/{{ $profile->qProfOID }}/remove" onclick="return confirm('Are you sure you want to remove profile from favorites?')"><i class="glyphicon glyphicon-remove"></i></a>
								</div>
	                    </div>
                  	</div>
				</div> <!-- /.col-md-6 col-sm-6 col-xs-12 -->

				@endforeach

				{!! str_replace('/?', '?', $profiles->appends(Input::only('tags', 'male', 'female', 'location', 'distance'))->render()) !!}

				@else
				<h3 class="page-body-title">No favorites...</h3>
				@endif

			</div> <!-- /.row favorite-row -->

        </div> <!-- /.search-form -->

	</div> <!-- /.row -->

</div> {{-- end .container --}}

@endsection
