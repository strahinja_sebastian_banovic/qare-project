@extends('app-front')

@section('content')

<div class="container acts">

	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-center">
			<h1 class="page-title invitesh1">Invites</h1>
		</div>
	</div>

	@if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
     @endif

	@if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
     @endif

     <div class="row chatinvites">
     	<div class="col-md-12 pull-right">
     		<a class="btn btn-info" href="{{ url('chatinvites') }}">
     			Chat invites
     		</a>
     	</div>
     </div>

			@if(count($allinvites) > 0)

			@foreach($allinvites as $invite)
				<?php
					if($invite->qProfOIDBookingCreated == $user->qProfOID)
					{
						$activity = DB::table('activities')->where('qActivOID', '=', $invite->qActivOID)->first();
						$userinvite = App\User::find($activity->qProfOIDCreated);
					}
					else {
						$activity = DB::table('activities')->where('qActivOID', '=', $invite->qActivOID)->first();
						$userinvite = $user;
					}

					if($activity->qLocation != '')
					{
						$location = explode(' | ', $activity->qLocation);
						$locationshow = $location[1];
					}
				?>

			<div class="row activity invites">

				<div class="act">

				    <div class="actimage" style="background:url({{ url('images/actimg.jpg') }});background-size:cover;">
						<div class="actimgbg"></div>
						
						<div class="actstatus">
							@if($invite->qStatus == 0)
								<p class="label label-info">Pending</p>
							@elseif($invite->qStatus == 1)
								<p class="label label-success">Accepted</p>
							@elseif($invite->qStatus == 2)
								<p class="label label-danger">Declined</p>
							@endif
						</div>

						<div class="acttags">
							<?php 
							$tags = DB::table('activtags')
							        ->where('qActivOID', '=', $activity->qActivOID)
							        ->get();
							 ?>
							 @if(count($tags) >0)
									@foreach($tags as $tag)
									<?php
										$qtag = DB::table('envtags')->where('qTagOID', '=', $tag->qTagOID)->where('qIsActive', '=', 1)->first();
									?>
									<p>{{ $qtag->qTagDesc }}</p>
									@endforeach
							@endif
						</div> <!-- /.acttags -->
						
						<?php 
						$checkActivFav = DB::table('activfavorites')->where('profile_id', Auth::user()->qProfOID)->where('activity_id', $activity->qActivOID)->first();
						?>
						
						<div class="activfavdiv">
							@if (empty($checkActivFav))
							<a href="#" class="makeactfav" data-actid="{{ $activity->qActivOID }}"><i class="glyphicon glyphicon-star-empty"></i> MAAK FAVORIET</a>
							@else
							<p class="activfav"><i class="glyphicon glyphicon-star"></i> FAVORIET</p>
							@endif
						</div>

					</div> <!-- /.actimage -->

					<div class="actinner">
					    
						<div class="userinfo">
							<?php
								$user = App\User::find($activity->qProfOIDCreated);

								$year = substr($user->qDateOfBirth ,0 ,4);
								if($year == '0000')
								{
									$year = 'Not set.';
								} else {
									$datetime1 = date_create(date('Y-m-d'));
									$datetime2 = date_create(date('Y-m-d', strtotime($user->qDateOfBirth)));
									$interval = date_diff($datetime1, $datetime2);
									$year = $interval->y;
								}

								 $commentCount = DB::table('activcomments')->where('qProfOIDActivCreated', $user->qProfOID)->count();
							?>
						    @if($user->qPicture)
								<img class="img-rounded" src="{{ url('images/users/thumbs/'.$user->qPicture) }}">
							@else
								@if($user->qGender==0 || $user->qGender==1)
									<img class="img-rounded" src="{{ url('img/user-male.png') }}">
								@else
									<img class="img-rounded" src="{{ url('img/user-female.png') }}">
								@endif
							@endif

							<div class="namereviews">
								<p class="actname">{{ $user->qNameFirst }}, {{ $year }}</p>
								<p class="actreviews">{{ $commentCount }} Reviews</p>
								<p class="link"><a href="{{ url('view-profile') }}/{{ $user->qProfOID }}">></a></p>
							</div> <!-- /.namereviews -->


							<div class="clear"></div>

						</div> <!-- /.userinfo -->

						<div class="actwat">
						    <div class="waar">
						    	<h4>WAAR</h4>
						        <?php 

						        if($activity->qLocation != ''){
								$place = explode(' | ', $activity->qLocation);
								}
						         ?>
						         <p>{{ $place[1] }}<p>

						    </div> <!-- /.waar -->
						    <div class="datum">
						    	<h4>DATUM EN TUD</h4>
						    	<p>{{ date('d M Y', strtotime($activity->qDate)).' '.substr($activity->qTime, 0, -3) }}</p>
						    </div>

						    <div class="duration">
						    	<h4>DURATION</h4>
						        <p>{{ $activity->qDuration }} hours</p>
						    </div> <!-- /.duration -->
						    <div class="clear"></div>
						</div> <!-- /.actwat -->

						<div class="actdesc">
							{{ $activity->qDescription }}
						</div>

					</div> <!-- /.actinner -->

				</div> <!-- /.act -->

			</div>


			@endforeach

			{!! str_replace('/?', '?', $allinvites->render()) !!}

			@else
			<h3 class="page-body-title">No invites at this moment...</h3>
			@endif

</div>

@endsection
