@extends('app-front')

@section('content')

<div class="container vulprofcontainer">

	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-center vulprofiel">
			<h1 class="page-title">Vul profiel</h1>
		</div>
	</div>

	@if(count($errors))
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ ($error) }}</p>
		@endforeach
	</div>
	@endif

	@if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
     @endif
	@if (session('phone_message'))
          <div class="alert alert-success">{{ session('phone_message') }}</div>
     @endif
	@if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
     @endif

	<form method="post" class="vulprofielform" enctype="multipart/form-data" action="{{ url('profile') }}">

		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="col-md-12">
					<input id="input-2" name="image" type="file" class="file-loading">
				</div>
			</div>
		</div>

		<br>

		<p class="vulprofp">Persoonlijke gegevens</p>

		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-xs-12 text-center">
				<div class="col-md-4 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control" name="name" value="{{ $user->qNameFirst }}" placeholder="First name">
				</div>
				<div class="col-md-4 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control" name="nameprefix" value="{{ $user->qNamePrefix }}" placeholder="Name prefix">
				</div>
				<div class="col-md-4 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control date" name="dateofbirth" value="@if($user->qDateOfBirth != '0000-00-00') {{ date('d.m.Y.', strtotime($user->qDateOfBirth)) }} @endif" placeholder="Date of birth">
				</div>
				<div class="col-md-4 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control" name="lastname" value="{{ $user->qNameLast }}" placeholder="Last name">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<div class="form-group customradio vulprofradios labelstyle">
							<label class="radio-inline">Gender: </label>
							&nbsp;&nbsp;&nbsp;
							<input type="radio" value="0" id="na" name="gender" @if($user->qGender == 0) {{ 'checked="ckecked"' }} @endif>
							<label for="na"><span></span> Not applicable</label>

							<input type="radio" value="1" id="male" name="gender" @if($user->qGender == 1) {{ 'checked="ckecked"' }} @endif>
							<label for="male"><span></span> Male</label>

							<input type="radio" value="2" id="female" name="gender" @if($user->qGender == 2) {{ 'checked="ckecked"' }} @endif>
							<label for="female"><span></span> Female</label>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-8 col-xs-12 col-md-offset-2">
				<div class="col-md-5 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" id="autocomplete" name="location" value="{{ $user->qCity }}" onFocus="geolocate()" class="form-control" placeholder="Search city">
					<input type="hidden" name="latlon" value="{{ $user->qLocation }}" id="longitude">
				</div>
				<div class="col-md-2 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control" name="postal_code" value="{{ $user->qZipCode }}" id="postal_code" placeholder="Zip code">
				</div>
				<div class="col-md-5 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control" name="address" value="{{ $user->qAddress }}" id="address" placeholder="Address">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-8 col-xs-12 col-md-offset-2">
				<div class="col-md-6 col-xs-12">
					<label>&nbsp;</label>
					<input type="email" class="form-control" name="email" value="{{ $user->qEmail }}" placeholder="Email address">
				</div>
				<div class="col-md-1 col-xs-12 emailverified">
					<label>&nbsp;</label>
					<label class="checkbox @if($user->qEmailVerifiedYN == 1) checkbox-success @endif">@if($user->qEmailVerifiedYN == 1) <i class="glyphicon glyphicon-ok"></i> @else <i class="glyphicon glyphicon-remove"></i> @endif  Verified</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-8 col-xs-12 col-md-offset-2">
				<div class="col-md-6 col-xs-12">
					<label>&nbsp;</label>
					<input type="text" class="form-control" name="phone" value="{{ $user->qPhoneNumber }}" placeholder="Phone number">
				</div>

				<?php
					$phonecheck = DB::table('phonecodes')->where('qProfOID', '=', Auth::user()->qProfOID)->first();
				?>
				<div class="col-md-1 col-xs-12">
					<label>&nbsp;</label>
					@if($user->qPhoneVerifiedYN == 1)
					<label class="checkbox"> @if($user->qPhoneVerifiedYN == 1) <i class="glyphicon glyphicon-ok"></i> @else <i class="glyphicon glyphicon-remove"></i> @endif  Verified</label>
					@else

						@if($phonecheck)
							<input type="text" name="phonecode" class="form-control" placeholder="Phone code" value="{{ old('phonecode') }}">
						@endif

					@endif
				</div>
				@if(!$phonecheck)
					<div class="col-md-12 col-xs-12 enterphone">
						<p style="font-size:12px;"><b>*Enter phone number to receive sms with verification code.</b></p>
					</div>
				@endif
			</div>
		</div>

		<br/>
		<p class="vulprofp">Mjin profiel</p>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12 col-xs-12">
					<textarea class="form-control" name="description" rows="5" placeholder="Describe yourself">{{ $user->qDescription }}</textarea>
				</div>
			</div>
		</div>
		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12 col-xs-12">
					<textarea class="form-control" name="interest" rows="5" placeholder="Interest">{{ $user->qInterest }}</textarea>
				</div>
			</div>
		</div>

		<br/>
		<p class="vulprofp">Interesse tags</p>

		<div class="row tagsbre">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12 col-xs-12">
					<div class="row input_fields_wrap">
						<div class="col-md-12 col-xs-12">
							<a class="add_field_button btn btn-primary btn-xs addnewtagbtn">Add a new tag</a>
							<label>&nbsp;</label>
						</div>

						<?php $i=0; ?>

						@if(count($tags) > 0)
							@foreach($tags as $tag)

							<?php
								$i++;
								$tagname = DB::table('envtags')->where('qTagOID', '=', $tag->qTagOID)->first();
							?>

							<div class="col-md-6 col-xs-12 tags" id="tagid-{{ $tag->qTagOID }}">
								<div class="input-group">
									<input type="text" name="tags[]" value="{{ $tagname->qTagDesc }}"  class="form-control tags" placeholder="Tag">
									<span class="input-group-btn">
										<a href="#" class="btn btn-default" type="button" onclick="if (confirm('Are you sure you want to delete this from tags?')) { delete_tag('{{ $tag->qTagOID }}'); } ">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</a>
									</span>
								</div>
							</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>

		<br/>
		<p class="vulprofp">Social Media</p>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<input type="text" name="facebook" class="form-control" value="{{ $user->qURLFacebook }}" placeholder="Facebook url">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<input type="text" name="twitter" class="form-control" value="{{ $user->qURLTwitter }}" placeholder="Twitter url">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<input type="text" name="linkedin" class="form-control" value="{{ $user->qURLLinkedIn }}" placeholder="LinkedIn url">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<input type="text" name="instagram" class="form-control" value="{{ $user->qURLInstagram }}" placeholder="Instagram url">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<input type="text" name="other" class="form-control" value="{{ $user->qURLOther }}" placeholder="Personal url">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12">
					<input type="text" name="idchecker" class="form-control" disabled="" placeholder="ID checker">
				</div>
			</div>
		</div>

		<br/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="col-md-12">
					<a class="privacy" href="{{ url('terms-and-conditions') }}" target="_blank">Privacy</a>
				</div>
			</div>
		</div>

		<hr/>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12 dashboard">
					<input type="submit" value="SAVE" class="btn btn-primary btn-block">
				</div>
			</div>
		</div>

	</form>

</div>

@endsection

@section('tailscripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&libraries=geometry,places&callback=initAutocomplete&language=en" async defer></script>
<script>

	var placeSearch, autocomplete;

	function initAutocomplete() {

		autocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('autocomplete')),
			{types: ['geocode']});

		autocomplete.addListener('place_changed', fillInAddress);
	}

	function fillInAddress() {
		var place = autocomplete.getPlace();
		$("#longitude").val(place.geometry.location.lat() + ', ' + place.geometry.location.lng());


		for (var i = 0; i < place.address_components.length; i++) {

			var addressType = place.address_components[i].types[0];

			if(addressType == 'locality')
			{
				$("#autocomplete").val(place.address_components[i].long_name);
			}
			if(addressType == 'postal_code')
			{
				$("#postal_code").val(place.address_components[i].long_name);
			}

		}
	}

	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}
</script>

<script type="text/javascript">

	function delete_tag(id) {
		$.ajax({
			url: '{{ url('delete-tag') }}',
			type: 'post',
			headers:
		    {
		        'X-CSRF-Token': $('input[name="_token"]').val()
		    },
			data: {'id' : id},
			success: function(response) {
				$("#tagid-"+id).remove();
			}
		});
	}



	$(document).ready(function() {
		var max_fields      = 10;
		var wrapper         = $(".input_fields_wrap");
		var add_button      = $(".add_field_button");

		var x = {{ $i }};
		$(add_button).click(function(e){
			e.preventDefault();
			if(x < max_fields){
				x++;
				$(wrapper).append('<div class="col-md-6 col-xs-12 tags"><div class="input-group"><input type="text" name="tags[]"  class="form-control" placeholder="Tag"><span class="input-group-btn remove_field"><a  href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></span></div></div>');
			}
		});

		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			e.preventDefault(); $(this).parent().parent('div').remove(); x--;
		})
	});
</script>

<script>
$(document).on('ready', function() {
    $("#input-2").fileinput(
    @if($user->qPicture)
	    {
	        initialPreview: [
	            '<img src="{{ url('images/users/'.$user->qPicture) }}" class="file-preview-image">'
	        ],
	        initialCaption: "{{ $user->qPicture }}"
	    }
	@endif
    );
});
</script>
@endsection
