-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2016 at 07:25 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qare`
--
CREATE DATABASE IF NOT EXISTS `qare` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `qare`;

-- --------------------------------------------------------

--
-- Table structure for table `activbookings`
--

DROP TABLE IF EXISTS `activbookings`;
CREATE TABLE `activbookings` (
  `qActivOID` int(11) NOT NULL,
  `qProfOIDBookingCreated` smallint(6) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qStatus` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `activcomments`
--

DROP TABLE IF EXISTS `activcomments`;
CREATE TABLE `activcomments` (
  `qActivOID` int(11) NOT NULL,
  `qActivCommentsNID` smallint(6) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qProfOIDActivCreated` int(11) NOT NULL,
  `qComment` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `qActivOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvOID` int(11) NOT NULL,
  `qProfOIDCreated` int(11) NOT NULL,
  `qTitle` char(50) COLLATE utf8_bin NOT NULL,
  `qDescription` varchar(255) COLLATE utf8_bin NOT NULL,
  `qImage` mediumblob NOT NULL,
  `qLocation` text COLLATE utf8_bin NOT NULL,
  `qDate` date NOT NULL,
  `qTime` time NOT NULL,
  `qPublicYN` tinyint(1) NOT NULL,
  `qOutsideYN` tinyint(1) NOT NULL,
  `qNofSpaces` tinyint(1) NOT NULL,
  `qURLLocation` varchar(255) COLLATE utf8_bin NOT NULL,
  `qDuration` tinyint(23) NOT NULL,
  `qPrice` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `activtags`
--

DROP TABLE IF EXISTS `activtags`;
CREATE TABLE `activtags` (
  `qActivOID` int(11) NOT NULL,
  `qTagOID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `enviroment`
--

DROP TABLE IF EXISTS `enviroment`;
CREATE TABLE `enviroment` (
  `qEnvOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvTitle` char(30) COLLATE utf8_bin NOT NULL,
  `qEnvDesc` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `envtags`
--

DROP TABLE IF EXISTS `envtags`;
CREATE TABLE `envtags` (
  `qTagOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvOID` int(11) NOT NULL,
  `qTagDesc` char(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `profcomments`
--

DROP TABLE IF EXISTS `profcomments`;
CREATE TABLE `profcomments` (
  `qProfOID` int(11) NOT NULL,
  `qProfCommentsNID` smallint(6) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qProfOIDCreated` int(11) NOT NULL,
  `qComment` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `proffavorites`
--

DROP TABLE IF EXISTS `proffavorites`;
CREATE TABLE `proffavorites` (
  `qProfOID` int(11) NOT NULL,
  `qProfFavoritesNID` smallint(6) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qTypeOfFavorite` tinyint(1) NOT NULL,
  `qOIDFavorite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `qProfOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvOID` int(11) NOT NULL,
  `qTypeOfProfile` smallint(6) NOT NULL,
  `qEmail` varchar(255) COLLATE utf8_bin NOT NULL,
  `qPassword` varchar(255) COLLATE utf8_bin NOT NULL,
  `qNameFirst` varchar(255) COLLATE utf8_bin NOT NULL,
  `qNamePrefix` varchar(255) COLLATE utf8_bin NOT NULL,
  `qNameLast` varchar(255) COLLATE utf8_bin NOT NULL,
  `qDateOfBirth` date NOT NULL,
  `qGender` tinyint(1) NOT NULL,
  `qAddress` varchar(255) COLLATE utf8_bin NOT NULL,
  `qZipCode` varchar(255) COLLATE utf8_bin NOT NULL,
  `qCity` varchar(255) COLLATE utf8_bin NOT NULL,
  `qPicture` mediumblob NOT NULL,
  `qPhoneNumber` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLFacebook` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLTwitter` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLLinkedIn` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLInstagram` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLOther` varchar(255) COLLATE utf8_bin NOT NULL,
  `qEmailFriend` varchar(255) COLLATE utf8_bin NOT NULL,
  `qEmailVerifiedYN` tinyint(1) NOT NULL,
  `qPhoneVerifiedYN` tinyint(1) NOT NULL,
  `qSocialMediaVerifiedYN` tinyint(1) NOT NULL,
  `qIDVerifiedYN` tinyint(1) NOT NULL,
  `qVOGVerifiedYN` tinyint(1) NOT NULL,
  `qProfileMemberLevel` tinyint(1) NOT NULL,
  `qProfileLevel` tinyint(1) NOT NULL,
  `qDescription` text COLLATE utf8_bin NOT NULL,
  `qInterest` text COLLATE utf8_bin NOT NULL,
  `qAnswers[5]` text COLLATE utf8_bin NOT NULL,
  `qPrefMaleYN` tinyint(1) NOT NULL,
  `qPrefFemaleYN` tinyint(1) NOT NULL,
  `qPrefInsideYN` tinyint(1) NOT NULL,
  `qPrefOutsideYN` tinyint(1) NOT NULL,
  `qPrefIndividualYN` tinyint(1) NOT NULL,
  `qPrefGroupYN` tinyint(1) NOT NULL,
  `qPrefDistance` int(11) NOT NULL,
  `qPrefProfileLevel` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `proftags`
--

DROP TABLE IF EXISTS `proftags`;
CREATE TABLE `proftags` (
  `qProfOID` int(11) NOT NULL,
  `qTagOID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activbookings`
--
ALTER TABLE `activbookings`
  ADD PRIMARY KEY (`qActivOID`,`qProfOIDBookingCreated`);

--
-- Indexes for table `activcomments`
--
ALTER TABLE `activcomments`
  ADD PRIMARY KEY (`qActivOID`,`qActivCommentsNID`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`qActivOID`),
  ADD UNIQUE KEY `qEnvOID` (`qEnvOID`,`qActivOID`);

--
-- Indexes for table `activtags`
--
ALTER TABLE `activtags`
  ADD PRIMARY KEY (`qActivOID`,`qTagOID`);

--
-- Indexes for table `enviroment`
--
ALTER TABLE `enviroment`
  ADD PRIMARY KEY (`qEnvOID`);

--
-- Indexes for table `envtags`
--
ALTER TABLE `envtags`
  ADD PRIMARY KEY (`qTagOID`),
  ADD UNIQUE KEY `qEnvOID` (`qEnvOID`,`qTagOID`);

--
-- Indexes for table `profcomments`
--
ALTER TABLE `profcomments`
  ADD PRIMARY KEY (`qProfOID`,`qProfCommentsNID`);

--
-- Indexes for table `proffavorites`
--
ALTER TABLE `proffavorites`
  ADD PRIMARY KEY (`qProfOID`,`qProfFavoritesNID`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`qProfOID`),
  ADD UNIQUE KEY `qProfOID` (`qProfOID`,`qEnvOID`);

--
-- Indexes for table `proftags`
--
ALTER TABLE `proftags`
  ADD PRIMARY KEY (`qProfOID`,`qTagOID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `qActivOID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `enviroment`
--
ALTER TABLE `enviroment`
  MODIFY `qEnvOID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `envtags`
--
ALTER TABLE `envtags`
  MODIFY `qTagOID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `qProfOID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
