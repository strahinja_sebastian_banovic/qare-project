<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'facebook' => [
        'client_id' => '528470814000918',
        'client_secret' => '4cdc592a21c4021213ee535b82a22c74',
        //'redirect' => 'http://ec2-54-175-52-112.compute-1.amazonaws.com/qare/qare-project/public/callback',
        'redirect' => 'http://ec2-52-91-110-115.compute-1.amazonaws.com/qare/qare-project/public/callback',
    ],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

];
